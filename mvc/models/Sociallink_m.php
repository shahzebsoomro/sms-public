<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sociallink_m extends MY_Model {

    protected $_table_name = 'sociallink';
    protected $_primary_key = 'sociallinkID';
    protected $_primary_filter = 'intval';
    protected $_order_by = "sociallinkID asc";

    function __construct() {
        parent::__construct();
    }

    function get_sociallink($array=NULL, $signal=FALSE) {
        $query = parent::get($array, $signal);
        return $query;
    }

    function get_single_sociallink($array) {
        $query = parent::get_single($array);
        return $query;
    }

    function get_order_by_sociallink($array=NULL) {
        $query = parent::get_order_by($array);
        return $query;
    }

    function insert_sociallink($array) {
        $id = parent::insert($array);
        return $id;
    }

    function update_sociallink($data, $id = NULL) {
        parent::update($data, $id);
        return $id;
    }

    public function delete_sociallink($id){
        parent::delete($id);
    }

    // public function get_user_social_info() {
    //     $this->db->select('*');
    //     $this->db->from('sociallink');
    //     $this->db->join('systemadmin','systemadmin.usertypeID = sociallink.usertypeID');
    //     $this->db->join('teacher','teacher.usertypeID = sociallink.usertypeID');
    //     $this->db->join('student','student.usertypeID = sociallink.usertypeID');
    //     $this->db->join('parents','parents.usertypeID = sociallink.usertypeID');
    //     $this->db->join('user','user.usertypeID = sociallink.usertypeID');
    //     $query = $this->db->get();
    //     return $query->result();
    // }

}
