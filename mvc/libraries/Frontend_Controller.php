<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property document_m $document_m
 * @property email_m $email_m
 * @property error_m $error_m
 */
class Frontend_Controller extends MY_Controller {
    /*
    | -----------------------------------------------------
    | PRODUCT NAME: 	INILABS SCHOOL MANAGEMENT SYSTEM
    | -----------------------------------------------------
    | AUTHOR:			INILABS TEAM
    | -----------------------------------------------------
    | EMAIL:			info@inilabs.net
    | -----------------------------------------------------
    | COPYRIGHT:		RESERVED BY INILABS IT
    | -----------------------------------------------------
    | WEBSITE:			http://inilabs.net
    | -----------------------------------------------------
    */

    private $_frontendTheme = '';
    private $_frontendThemePath = '';
    private $_frontendThemeBasePath = '';


    protected $bladeView;

    function __construct () {
        parent::__construct();
        $this->load->driver('cache', array('adapter' => 'file'));
        $this->load->library('blade');
        $this->load->library("session");
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('language');
        $this->load->helper('date');
        $this->load->helper('form');
        $this->load->helper('traffic');
        $this->load->helper("frontenddata");
        $this->load->model('frontend_setting_m');
        $this->load->model('setting_m');
        $this->load->model('pages_m');
        $this->load->model('posts_m');
        $this->load->model('fmenu_relation_m');
        $this->load->model('fmenu_m');
        $this->load->model('event_m');
        $this->load->model('teacher_m');
        $this->load->model('notice_m');
        $this->load->model('sociallink_m');


        /* Start All Data Call */
        $this->data['events'] = $this->event_m->get_order_by_event();
        $this->data['teachers'] = $this->teacher_m->get_teacher();
        $this->data['notices'] = $this->notice_m->get_order_by_notice(array('schoolyearID' => frontendData::get_backend('school_year')));
        $this->data['sociallink'] = pluck_multi_array_key($this->sociallink_m->get_sociallink(), 'obj', 'usertypeID', 'userID');
        /* Close All Data Call */

        $this->data['backend_setting'] = $this->setting_m->get_setting();
        $this->data['frontend_setting'] = $this->frontend_setting_m->get_frontend_setting();
        $this->data['frontend_topbar'] = $this->fmenu_m->get_single_fmenu(array('topbar' => 1));
        $this->data['frontend_social'] = $this->fmenu_m->get_single_fmenu(array('social' => 1));
        

        $this->data['homepage'] = $this->pages_m->get_one($this->data['frontend_topbar']);

        $this->data['forntend_pages'] = pluck($this->pages_m->get_pages(), 'obj', 'pagesID');
        $this->data['forntend_posts'] = pluck($this->posts_m->get_posts(), 'obj', 'postsID');
        $this->data['menu'] = $this->callMenu();

        $this->_frontendTheme = strtolower($this->data["backend_setting"]->frontend_theme);;
        $this->_frontendThemePath = 'frontend/'. $this->_frontendTheme.'/';
        $this->_frontendThemeBasePath = FCPATH.'frontend/'. $this->_frontendTheme.'/';

        $this->blade->load_view_root($this->_frontendThemeBasePath);
        $this->bladeView = $this->blade;
        $this->bladeView->set('backend', $this->data['backend_setting']);
        $this->bladeView->set('frontend', $this->data['frontend_setting']);
        $this->bladeView->set('frontendThemePath', $this->_frontendThemePath);

        // $this->bladeView->set('frontendTopbar', $this->data['frontend_topbar']);
        // $this->bladeView->set('frontendSocail', $this->data['frontend_social']);


        if(count($this->data['homepage'])) {
            $this->data['homepageTitle'] = $this->data['homepage']->menu_label;
            $this->bladeView->set('homepageTitle', $this->data['homepage']->menu_label);
            if($this->data['homepage']->menu_typeID == 1) {
                $page = $this->pages_m->get_single_pages(array('pagesID' => $this->data['homepage']->menu_pagesID));
                $this->data['homepage'] = $page;
                $this->data['homepageType'] = 'page';
                $this->bladeView->set('homepage', $page); 
                $this->bladeView->set('homepageType', 'page'); 
            } elseif($this->data['homepage']->menu_typeID == 2) {
                $post = $this->posts_m->get_single_posts(array('postsID' => $this->data['homepage']->menu_pagesID));
                $this->data['homepage'] = $post;
                $this->data['homepageType'] = 'post';
                $this->bladeView->set('homepage', $post);
                $this->bladeView->set('homepageType', 'post'); 
            } else {
                $nonehomepage = (object) array('url' => '');
                $this->data['homepage'] = $nonehomepage;
                $this->bladeView->set('homepage', $nonehomepage);
                $this->bladeView->set('homepageType', 'none'); 
            }   
        } else {
            $nonehomepage = (object) array('url' => '');
            $this->data['homepage'] = $nonehomepage;
            $this->data['homepageTitle'] = '';
            $this->bladeView->set('homepage', $nonehomepage);
            $this->bladeView->set('homepageType', 'none');
            $this->bladeView->set('homepageTitle', $this->data['homepageTitle']);
        }

        $this->bladeView->set('events', $this->data['events']);
        $this->bladeView->set('teachers', $this->data['teachers']);
        $this->bladeView->set('notices', $this->data['notices']);
        $this->bladeView->set('sociallink', $this->data['sociallink']);
        $this->bladeView->set('fpages', $this->data['forntend_pages']);
        $this->bladeView->set('fposts', $this->data['forntend_posts']);
        $this->bladeView->set('menu', $this->data['menu']);

    }

    private function callMenu() {
        $returnArray = [];
        $frontendTopbarMenu = [];
        $frontendSocialMenu = [];
        $frontendTopbarQueryMenus = [];
        $frontendSocialQueryMenus = [];
        if(count($this->data['frontend_topbar'])) {
            $frontendTopbarQueryMenus = $this->fmenu_relation_m->get_order_by_fmenu_relation(array('fmenuID' => $this->data['frontend_topbar']->fmenuID));
            $frontendTopbarMenu = $this->orderMenu($frontendTopbarQueryMenus);
        }

        if(count($this->data['frontend_topbar'])) {
            if($this->data['frontend_topbar']->social == 1) {
                $frontendSocialMenu = $frontendTopbarMenu;
                $frontendSocialQueryMenus = $frontendTopbarQueryMenus;
            } else {
                if(count($this->data['frontend_social'])) {
                    $frontendSocialQueryMenus = $this->fmenu_relation_m->get_order_by_fmenu_relation(array('fmenuID' => $this->data['frontend_social']->fmenuID));
                    $frontendSocialMenu = $this->orderMenu($frontendSocialQueryMenus);
                }
            }
        } else {
            if(count($this->data['frontend_social'])) {
                $frontendSocialQueryMenus = $this->fmenu_relation_m->get_order_by_fmenu_relation(array('fmenuID' => $this->data['frontend_social']->fmenuID));
                $frontendSocialMenu = $this->orderMenu($frontendSocialQueryMenus);
            }
        }

        $returnArray = array('frontendTopbarMenus' => $frontendTopbarMenu, 'frontendSocialMenus' => $frontendSocialMenu, 'frontendTopbarQueryMenus' => $frontendTopbarQueryMenus, 'frontendSocialQueryMenus' => $frontendSocialQueryMenus);

        return $returnArray;
    }

    private function orderMenu($elements) {
        $mergeelements = [];
        if(count($elements)) {
            $elements = json_decode(json_encode($elements), true);

            if(count($elements)) {
                foreach ($elements as $elementkey => $element) {
                    if($element['menu_parentID'] == 0) {
                        $mergeelements[] = $element;
                        unset($elements[$elementkey]);
                    }
                }

                foreach ($elements as $elementkey => $element) {
                    if(count($mergeelements)) {
                        foreach ($mergeelements as $mergeelementkey =>  $mergeelement) {
                            if($element['menu_rand_parentID'] == $mergeelement['menu_rand']) {
                                $mergeelements[$mergeelementkey]['child'][] = $element;         
                                unset($elements[$elementkey]);
                            }       
                        }
                    }
                }
                
                foreach ($elements as $elementkey => $element) {
                    if(count($mergeelements)) {
                        foreach ($mergeelements as $mergeelementkey =>  $mergeelement) {
                            if(isset($mergeelement['child'])) {
                                if(count($mergeelement['child'])) {
                                    foreach ($mergeelement['child'] as $secandlayerkey => $secandlayer) {
                                        if($secandlayer['menu_rand'] == $element['menu_rand_parentID']) {
                                            $mergeelements[$mergeelementkey]['child'][$secandlayerkey]['child'][] = $element; 
                                        }
                                    }
                                }
                            }       
                        }
                    }
                }
            }
        }

        return $mergeelements;
    }
}

