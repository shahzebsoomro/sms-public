<?php

$lang['panel_title'] = "Permissionlog";
$lang['add_title'] = "Menambah A Permissionlog";
$lang['slno'] = "#";
$lang['permissionlog_name'] = "Nama";
$lang['permissionlog_description'] = "Deskripsi";
$lang['permissionlog_active'] = "Aktif";
$lang['permissionlog_add'] = "Tambahkan";
$lang['action'] = "Aksi";
$lang['view'] = "Lihat";
$lang['edit'] = "Edit";
$lang['delete'] = "Menghapus";
$lang['print'] = "Cetak";
$lang['pdf_preview'] = "Pdf Preview";
$lang["mail"] = "Kirim Pdf Untuk E-mail";
$lang['add_class'] = "Tambahkan Permissionlog";
$lang['update_class'] = "Update Permissionlog";
$lang['to'] = "Untuk";
$lang['subject'] = "Subjek";
$lang['message'] = "Pesan";
$lang['send'] = "Kirim";
$lang['mail_to'] = "Di Bidang Yang Diperlukan.";
$lang['mail_valid'] = "Di Bidang Harus Mengandung A Valid Email Alamat.";
$lang['mail_subject'] = "Subject Bidang Yang Diperlukan.";
$lang['mail_success'] = "Email Kirim Berhasil%2c";
$lang['mail_error'] = "Oops%2c Email Tidak Mengirim%2c";
