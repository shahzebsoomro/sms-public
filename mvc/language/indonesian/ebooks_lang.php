<?php

$lang['panel_title'] = "E-buku";
$lang['slno'] = "#";
$lang['ebooks_name'] = "Nama";
$lang['ebooks_author'] = "Penulis";
$lang['ebooks_classes'] = "Kelas";
$lang['ebooks_authority'] = "Otoritas";
$lang['ebooks_private'] = "Pribadi";
$lang['ebooks_cover_photo'] = "Cover Foto";
$lang['ebooks_file'] = "File";
$lang['action'] = "Aksi";
$lang['ebooks_select_class'] = "Pilih Kelas";
$lang['ebooks_select_department'] = "Pilih Departemen";
$lang['ebooks_select_teacher'] = "Pilih Guru";
$lang['view'] = "Lihat";
$lang['edit'] = "Edit";
$lang['delete'] = "";
$lang['add_ebooks'] = "Tambahkan Ebook";
$lang['add_title'] = "Tambahkan Ebook";
$lang['update_ebooks'] = "Update Ebook";
