<?php

$lang['panel_title'] = "Frontend Menu";
$lang['add_title'] = "Menambah A Kegiatan";
$lang['slno'] = "#";
$lang['activities_title'] = "Judul";
$lang['activities_description'] = "Deskripsi";
$lang['activities_students'] = "Siswa";
$lang['activities_shared_publicly'] = "Bersama Publik";
$lang['file_browse'] = "Browse File";
$lang['attachment'] = "Lampiran";
$lang['clear'] = "Jelas";
$lang['activities_time_frame'] = "Waktu Frame";
$lang['activities_time_from'] = "Waktu Dari";
$lang['activities_time_to'] = "Waktu Untuk";
$lang['activities_time_at'] = "Waktu Di";
$lang['students'] = "Mahasiswa Daftar";
$lang['activities_create_date'] = "Membuat Tanggal";
$lang['action'] = "Aksi";
$lang['edit'] = "Edit";
$lang['delete'] = "Menghapus";
$lang['add_activities'] = "Tambahkan Kegiatan";
$lang['update_activities'] = "Update Kegiatan";
$lang['frontendmenu_edit_menus'] = "Edit Menu";
$lang['frontendmenu_select_a_menu_to_edit'] = "Pilih  Menu Untuk Edit";
$lang['frontendmenu_manage_locations'] = "Mengelola Loacations";
$lang['frontendmenu_top_menu'] = "";
$lang['frontendmenu_social_links_menu'] = "Sosial Link Menu";
$lang['frontendmenu_select'] = "Pilih";
$lang['frontendmenu_pages'] = "Halaman";
$lang['frontendmenu_posts'] = "Posting";
$lang['frontendmenu_custom_links'] = "Custom Link";
$lang['frontendmenu_custom_link'] = "";
$lang['frontendmenu_view_all'] = "Lihat Semua";
$lang['frontendmenu_select_all'] = "Pilih Semua";
$lang['frontendmenu_url'] = "Url";
$lang['frontendmenu_link_text'] = "Link Teks";
$lang['frontendmenu_pending'] = "Pending";
$lang['frontendmenu_page'] = "Halaman";
$lang['frontendmenu_post'] = "Post";
$lang['frontendmenu_navigation_label'] = "Navigasi Label";
$lang['frontendmenu_move'] = "Bergerak";
$lang['frontendmenu_up_one'] = "Up Satu";
$lang['frontendmenu_down_one'] = "Down Satu";
$lang['frontendmenu_orginal'] = "Asli";
$lang['frontendmenu_no_label'] = "Tidak Ada Label";
$lang['frontendmenu_remove'] = "Menghapus";
$lang['frontendmenu_cancel'] = "Membatalkan";
$lang['frontendmenu_add_to_menu'] = "Tambahkan Untuk Menu";
$lang['frontendmenu_save_menu'] = "Menyimpan Menu";
$lang['frontendmenu_save_changes'] = "Menyimpan Perubahan";
$lang['frontendmenu_menu_name'] = "Menu Nama";
$lang['frontendmenu_menu_structure'] = "Menu Struktur";
$lang['frontendmenu_menu_structure_desc'] = "Drag Each Item Into The Order You Prefer. Click The Arrow On The Right Of The Item To Reveal Additional Configuration Options.";
$lang['frontendmenu_display_location'] = "Tampilan Lokasi";
$lang['frontendmenu_menu_settings'] = "Menu Pengaturan";
$lang['frontendmenu_delete_menu'] = "Menghapus Menu";
$lang['frontendmenu_select_a_menu'] = "Pilih  Menu";
$lang['frontendmenu_error_label'] = "Label Bidang Tidak Boleh 253 Karakter Di Panjang.";
$lang['frontendmenu_error_menu'] = "Silahkan Buat Yang Menu.";
$lang['frontendmenu_or'] = "Atau";
$lang['frontendmenu_create_a_new_menu'] = "Buat Yang Baru Menu.";
$lang['frontendmenu_create_menu'] = "Membuat Menu";
$lang['frontendmenu_create_menu_desc'] = "Memberikan  Menu Sebuah Nama%2c Dan Klik Buat Menu.";
$lang['frontendmenu_edit_your_menu_below'] = "Edit  Menu Di Bawah Ini";
$lang['frontendmenu_error_menu_required'] = "Menu Nama Bidang Yang Diperlukan.";
$lang['frontendmenu_error_menu_length'] = "Menu Nama Bidang Tidak Boleh 128 Karakter Di Panjang.";
