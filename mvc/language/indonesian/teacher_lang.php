<?php

$lang['panel_title'] = "Guru";
$lang['add_title'] = "Menambah A Guru";
$lang['slno'] = "#";
$lang['teacher_photo'] = "Foto";
$lang['teacher_name'] = "Nama";
$lang['teacher_designation'] = "Penunjukan";
$lang['teacher_email'] = "Email";
$lang['teacher_dob'] = "Tanggal  Kelahiran";
$lang['teacher_sex'] = "Jenis Kelamin";
$lang['teacher_sex_male'] = "Laki-laki";
$lang['teacher_sex_female'] = "";
$lang['teacher_religion'] = "Agama";
$lang['teacher_phone'] = "Telepon";
$lang['teacher_address'] = "Alamat";
$lang['teacher_jod'] = "Bergabung Tanggal";
$lang['teacher_username'] = "Username";
$lang['teacher_password'] = "Password";
$lang['teacher_status'] = "Status";
$lang['teacher_file_browse'] = "File Isi";
$lang['teacher_clear'] = "Jelas";
$lang['teacher_profile'] = "Profil";
$lang['teacher_routine'] = "Rutin";
$lang['teacher_attendance'] = "Absensi";
$lang['teacher_mark'] = "Mark";
$lang['teacher_invoice'] = "Faktur";
$lang['teacher_salary'] = "Gaji";
$lang['teacher_payment'] = "Pembayaran";
$lang['teacher_document'] = "Dokumen";
$lang['action'] = "Aksi";
$lang['view'] = "Lihat";
$lang['edit'] = "Edit";
$lang['delete'] = "Menghapus";
$lang['print'] = "Cetak";
$lang['pdf_preview'] = "Pdf Preview";
$lang['idcard'] = "Id Kartu";
$lang['mail'] = "Kirim Pdf Untuk E-mail";
$lang['download'] = "Download";
$lang['personal_information'] = "Pribadi Informasi";
$lang['add_teacher'] = "Tambahkan Guru";
$lang['update_teacher'] = "Update Guru";
$lang['to'] = "Untuk";
$lang['subject'] = "Subjek";
$lang['message'] = "Pesan";
$lang['send'] = "Kirim";
$lang['mail_to'] = "Di Bidang Yang Diperlukan.";
$lang['mail_valid'] = "Di Bidang Harus Mengandung A Valid Email Alamat.";
$lang['mail_subject'] = "Subject Bidang Yang Diperlukan.";
$lang['mail_success'] = "Email Kirim Berhasil%2c";
$lang['mail_error'] = "Oops%2c Email Tidak Mengirim%2c";
$lang['sunday'] = "Minggu";
$lang['monday'] = "Senin";
$lang['tuesday'] = "Selasa";
$lang['wednesday'] = "Rabu";
$lang['thursday'] = "Kamis";
$lang['friday'] = "Jumat";
$lang['saturday'] = "Sabtu";
$lang["teacher_day"] = "Hari";
$lang["teacher_period"] = "Periode";
$lang["teacher_subject"] = "Subjek";
$lang["teacher_room"] = "Kamar";
$lang["teacher_class"] = "Kelas";
$lang["teacher_section"] = "Bagian";
$lang['teacher_1'] = "Satu";
$lang['teacher_2'] = "Dua";
$lang['teacher_3'] = "Tiga";
$lang['teacher_4'] = "Empat";
$lang['teacher_5'] = "Lima";
$lang['teacher_6'] = "Enam";
$lang['teacher_7'] = "Tujuh";
$lang['teacher_8'] = "Delapan";
$lang['teacher_9'] = "Sembilan";
$lang['teacher_10'] = "Sepuluh";
$lang['teacher_11'] = "Sebelas";
$lang['teacher_12'] = "Dua Belas";
$lang['teacher_13'] = "Tiga Belas";
$lang['teacher_14'] = "Empat Belas";
$lang['teacher_15'] = "Lima Belas";
$lang['teacher_16'] = "Enam Belas";
$lang['teacher_17'] = "Tujuh Belas";
$lang['teacher_18'] = "Delapan Belas";
$lang['teacher_19'] = "Sembilan Belas";
$lang['teacher_20'] = "Dua Puluh";
$lang['teacher_21'] = "Dua Puluh Satu";
$lang['teacher_22'] = "Dua Puluh Dua";
$lang['teacher_23'] = "Dua Puluh Tiga";
$lang['teacher_24'] = "Dua Puluh Empat";
$lang['teacher_25'] = "Dua Puluh Lima";
$lang['teacher_26'] = "Dua Puluh Enam";
$lang['teacher_27'] = "Dua Puluh Tujuh";
$lang['teacher_28'] = "Dua Puluh Delapan";
$lang['teacher_29'] = "Dua Puluh Sembilan";
$lang['teacher_30'] = "Tiga Puluh";
$lang['teacher_31'] = "Tiga Puluh Satu";
$lang['teacher_salary_grades'] = "Gaji Nilai";
$lang['teacher_basic_salary'] = "Dasar Gaji";
$lang['teacher_overtime_rate'] = "Lembur Tingkat %28 Per Jam%29";
$lang['teacher_overtime_rate_not_hour'] = "Lembur Tingkat";
$lang['teacher_allowances'] = "Tunjangan";
$lang['teacher_deductions'] = "Dllowances";
$lang['teacher_total_salary_details'] = "Total Gaji Rincian";
$lang['teacher_gross_salary'] = "Kotor Gaji";
$lang['teacher_total_deduction'] = "Total Pengurang";
$lang['teacher_net_salary'] = "Net Gaji";
$lang['teacher_hourly_rate'] = "Per Tingkat";
$lang['teacher_net_hourly_rate'] = "Net Jam Tingkat";
$lang['teacher_net_salary_hourly'] = "Net Gaji %28hourly%29";
$lang['teacher_month'] = "Bulan";
$lang['teacher_payment_amount'] = "Pembayaran Jumlah";
$lang['teacher_total'] = "Total";
$lang['teacher_add_document'] = "Tambahkan Dokumen";
$lang['teacher_document_upload'] = "Dokumen Tanggal";
$lang['teacher_title'] = "Judul";
$lang['teacher_file'] = "File";
$lang['teacher_upload'] = "Tanggal";
$lang['teacher_title_required'] = "Judul Bidang Yang Diperlukan.";
$lang['teacher_file_required'] = "File Bidang Yang Diperlukan.";
$lang['teacher_date'] = "Tanggal";
$lang['teacher_to'] = "Untuk";
$lang['teacher_message'] = "Pesan";
$lang['teacher_teacherID'] = "Guru Id";
$lang['teacher_data_not_found'] = "Don%27t Memiliki  Data.";
$lang['teacher_permissionmethod'] = "Metode Yang Tidak Boleh";
$lang['teacher_permission'] = "Izin Tidak Boleh";
$lang['teacher_total_holiday'] = "Total Liburan";
$lang['teacher_total_weekenday'] = "Total Weekenday";
$lang['teacher_total_present'] = "Total Hadir";
$lang['teacher_total_latewithexcuse'] = "Akhir Total Alasan";
$lang['teacher_total_late'] = "Total Terlambat";
$lang['teacher_total_absent'] = "Total Absen";
$lang['teacher_total_leave'] = "Total Meninggalkan";
