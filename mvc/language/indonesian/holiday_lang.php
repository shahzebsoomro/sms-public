<?php

$lang['panel_title'] = "Liburan";
$lang['add_title'] = "Menambah A Liburan";
$lang['slno'] = "#";
$lang['holiday_title'] = "Judul";
$lang['holiday_details'] = "Detail";
$lang['holiday_date'] = "Tanggal";
$lang['holiday_fdate'] = "Dari Tanggal";
$lang['holiday_tdate'] = "Untuk Tanggal";
$lang['holiday_photo'] = "Foto";
$lang['action'] = "Aksi";
$lang['holiday_file_browse'] = "File Isi";
$lang['holiday_clear'] = "Jelas";
$lang['holiday_header'] = "Liburan Rincian";
$lang['view'] = "Lihat";
$lang['edit'] = "Edit";
$lang['delete'] = "Menghapus";
$lang['print'] = "Cetak";
$lang['pdf_preview'] = "Pdf Preview";
$lang["mail"] = "Kirim Pdf Untuk E-mail";
$lang['add_class'] = "Tambahkan Liburan";
$lang['update_class'] = "Update Liburan";
$lang['to'] = "Untuk";
$lang['subject'] = "Subjek";
$lang['message'] = "Pesan";
$lang['send'] = "Kirim";
$lang['mail_to'] = "";
$lang['mail_valid'] = "Di Bidang Harus Mengandung A Valid Email Alamat.";
$lang['mail_subject'] = "Subject Bidang Yang Diperlukan.";
$lang['mail_success'] = "Email Kirim Berhasil%2c";
$lang['mail_error'] = "Oops%2c Email Tidak Mengirim%2c";
$lang['holiday_to'] = "Untuk";
$lang['holiday_subject'] = "Subjek";
$lang['holiday_message'] = "Pesan";
$lang['holiday_holidayID'] = "Liburan Id";
$lang['holiday_data_not_found'] = "Don%27t Memiliki  Data.";
$lang['holiday_permissionmethod'] = "Metode Yang Tidak Boleh";
$lang['holiday_permission'] = "Izin Tidak Boleh";
