<?php

$lang['panel_title'] = "Kullan�c�";
$lang['add_title'] = "Kullan�c� Ekle";
$lang['slno'] = "#";
$lang['user_photo'] = "Foto��raf";
$lang['user_name'] = "Ad�";
$lang['user_email'] = "E-posta";
$lang['user_dob'] = "Tarihi Do��um";
$lang['user_sex'] = "Cinsiyet";
$lang['user_sex_male'] = "Erkek";
$lang['user_sex_female'] = "Kad�n";
$lang['user_religion'] = "Din";
$lang['user_phone'] = "Telefon";
$lang['user_address'] = "Adres";
$lang['user_jod'] = "Kat�lma Tarih";
$lang['user_usertype'] = "Rol";
$lang['user_username'] = "Kullan�c� Ad�";
$lang['user_password'] = "Şifre";
$lang['user_status'] = "Durumu";
$lang['user_designation'] = "Atama";
$lang['user_designation'] = "Atama";
$lang['user_select_usertype'] = "Se�in Rol";
$lang['user_file_browse'] = "Dosyas� G�zat";
$lang['user_clear'] = "A��k";
$lang['user_quick_add'] = "H�zl� Ekleyin";
$lang['action'] = "Eylem";
$lang['view'] = "G�r�n�m";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['pdf_preview'] = "Pdf Önizleme";
$lang['idcard'] = "K�ml�k Kart�";
$lang['print'] = "Bask�";
$lang['download'] = "İndir";
$lang["mail"] = "Posta  Pdf G�nder";
$lang['personal_information'] = "Ki��isel Bilgi";
$lang['add_user'] = "Kullan�c� Ekle";
$lang['update_user'] = "G�ncelleme Kullan�c�";
$lang['to'] = "İ�in";
$lang['subject'] = "Konu";
$lang['message'] = "Mesaj";
$lang['send'] = "G�nder";
$lang['mail_to'] = "Alan   Gereklidir.";
$lang['mail_valid'] = "Alan    Ge�erli E-posta Adresi Gerekir.";
$lang['mail_subject'] = "Konu Alan  Gereklidir.";
$lang['mail_success'] = "E-posta 2c Ba��ar�yla%g�nder";
$lang['mail_error'] = "Oops%2c E-posta 2c%g�ndermeyecek";
$lang['user_title'] = "Ba��l�k";
$lang['user_date'] = "Tarih";
$lang['user_add_document'] = "Belge Eklemek";
$lang['user_document_upload'] = "Belge Upload";
$lang['user_file'] = "Dosya";
$lang['user_upload'] = "Y�kleme";
$lang['user_title_required'] = "Ba��l�k Alan  Gereklidir.";
$lang['user_file_required'] = "Dosya Alan  Gereklidir.";
$lang['user_profile'] = "Profil";
$lang['user_attendance'] = "Kat�l�m";
$lang['user_salary'] = "Maa��";
$lang['user_payment'] = "Ödeme";
$lang['user_document'] = "Belge";
$lang['user_1'] = "Bir";
$lang['user_2'] = "İki";
$lang['user_3'] = "Ü�";
$lang['user_4'] = "D�rt";
$lang['user_5'] = "Be��";
$lang['user_6'] = "Alt�";
$lang['user_7'] = "Yedi";
$lang['user_8'] = "Sekiz";
$lang['user_9'] = "Dokuz";
$lang['user_10'] = "On";
$lang['user_11'] = "On Bir";
$lang['user_12'] = "On Iki";
$lang['user_13'] = "On ü�";
$lang['user_14'] = "On D�rt";
$lang['user_15'] = "On Be��";
$lang['user_16'] = "On Alt�";
$lang['user_17'] = "On Yedi";
$lang['user_18'] = "On Sekiz";
$lang['user_19'] = "On Dokuz";
$lang['user_20'] = "Yirmi";
$lang['user_21'] = "Yirmi Bir";
$lang['user_22'] = "Yirmi Iki";
$lang['user_23'] = "Yirmi ü�";
$lang['user_24'] = "Yirmi D�rt";
$lang['user_25'] = "Yirmi Be��";
$lang['user_26'] = "Yirmi Alt�";
$lang['user_27'] = "Yirmi Yedi";
$lang['user_28'] = "Yirmi Sekiz";
$lang['user_29'] = "Yirmi Dokuz";
$lang['user_30'] = "Otuz";
$lang['user_31'] = "Otuz Bir";
$lang['user_salary_grades'] = "Maa�� Notlar�";
$lang['user_basic_salary'] = "Temel Maa��";
$lang['user_overtime_rate'] = "Ba���na Fazla Mesai Oran� %28 %29";
$lang['user_overtime_rate_not_hour'] = "Fazla Mesai Oran�";
$lang['user_allowances'] = "Ödenekleri";
$lang['user_deductions'] = "Dllowances";
$lang['user_total_salary_details'] = "Toplam Maa�� Detaylar";
$lang['user_gross_salary'] = "Br�t Maa��";
$lang['user_total_deduction'] = "Toplam İndirimi";
$lang['user_net_salary'] = "Net Maa��";
$lang['user_hourly_rate'] = "Saatlik Oran�";
$lang['user_net_hourly_rate'] = "Net Saatlik Oran�";
$lang['user_net_salary_hourly'] = "Net Maa�� %28hourly%29";
$lang['user_month'] = "Ay";
$lang['user_payment_amount'] = "Ödeme Tutar�";
$lang['user_total'] = "Toplam";
$lang['user_to'] = "İ�in";
$lang['user_subject'] = "Konu";
$lang['user_message'] = "Mesaj";
$lang['user_userID'] = "Kullan�c� Id";
$lang['user_data_not_found'] = "Yok%27t Herhangi Veri .";
$lang['user_permissionmethod'] = "Y�ntem  İzin Verilmiyor";
$lang['user_permission'] = "İzin  De��il";
$lang['user_type'] = "Yaz�n";
$lang['user_total_holiday'] = "Toplam Tatil";
$lang['user_total_weekenday'] = "Toplam Weekenday";
$lang['user_total_present'] = "Toplam Hediye";
$lang['user_total_latewithexcuse'] = "Bahane İle Toplam Ge�";
$lang['user_total_late'] = "Toplam Ge�";
$lang['user_total_absent'] = "Toplam Devams�zl�k";
