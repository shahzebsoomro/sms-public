<?php

$lang['panel_title'] = "S�nava";
$lang['add_title'] = "Eklemek A S�navlar�na";
$lang['slno'] = "#";
$lang['take_exam_name'] = "Ad�";
$lang['take_exam_duration'] = "S�resi";
$lang['take_exam_no_question'] = "Don%27t Herhangi Soru Var";
$lang['take_exam_mark'] = "Mark";
$lang['take_exam_add'] = "Ekle";
$lang['take_exam_question'] = "Soru";
$lang['take_exam_of'] = "Biri";
$lang['take_exam_time_status'] = "Zaman Durum";
$lang['take_exam_total_time'] = "Toplam Zaman";
$lang['take_exam_summary'] = "Özet";
$lang['take_exam_answered'] = "Cevap";
$lang['take_exam_marked'] = "İ��aretli";
$lang['take_exam_not_answer'] = "Cevap Yok";
$lang['take_exam_not_visited'] = "Ziyaret";
$lang['take_exam_next'] = "Gelecek";
$lang['take_exam_previous'] = "Önceki";
$lang['take_exam_finish'] = "Bitir";
$lang['take_exam_clear_answer'] = "A��k Cevap";
$lang['take_exam_mark_review'] = "İnceleme %26 Mark Sonraki";
$lang['take_exam_start_exam'] = "Ba��lat S�nav";
$lang['take_exam_exam_name'] = "S�nav İsim";
$lang['take_exam_warning'] = "Uyar�";
$lang['take_exam_page_refresh'] = "Do Not Bas�n %2frefresh D���mesine";
$lang['action'] = "Eylem";
$lang['take_exam_roll'] = "Rulo";
$lang['take_exam_class'] = "S�n�f";
$lang['take_exam_section'] = "B�l�m";
$lang['take_exam_total_question'] = "Toplam Soru";
$lang['take_exam_total_answer'] = "Toplam Cevap";
$lang['take_exam_total_current_answer'] = "Toplam Currect Cevap";
$lang['take_exam_total_mark'] = "Toplam İ��areti";
$lang['take_exam_total_obtained_mark'] = "Toplam İ��areti Elde";
$lang['take_exam_total_percentage'] = "Toplam Y�zdesi";
$lang['take_exam_pass'] = "Pas";
$lang['take_exam_fail'] = "Ba�arisiz";
$lang['take_exam_examstatus'] = "S�nav Durum";
$lang['take_exam_onetime'] = "Zaman";
$lang['take_exam_multipletime'] = "Birden Fazla Zaman";
$lang['take_exam_taken'] = "Al�nan";
$lang['take_exam_anytime'] = "Her Zaman";
$lang['take_exam_today_only'] = "Bug�n Sadece";
$lang['take_exam_retaken'] = "Geri";
$lang['take_exam_expired'] = "S�resi Dolmu��";
$lang['take_exam_running'] = "Çal���an";
$lang['take_exam_attend'] = "Kat�lmak";
$lang['take_exam_upcoming'] = "Gelecek";
$lang['take_exam_todays'] = "G�n�m�z";
$lang['take_exam_days'] = "G�n";
$lang['take_exam_exam_not_found'] = "S�nav Bilgi De��il Buldu";
$lang['view'] = "G�r�n�m";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['print'] = "Bask�";
$lang['pdf_preview'] = "Pdf Önizleme";
$lang["mail"] = "Posta  Pdf G�nder";
$lang['add_class'] = "Ekle  S�nava";
$lang['update_class'] = "G�ncelleme  S�nava";
$lang['to'] = "İ�in";
$lang['subject'] = "Konu";
$lang['message'] = "Mesaj";
$lang['send'] = "G�nder";
$lang['mail_to'] = "Alan   Gereklidir.";
$lang['mail_valid'] = "Alan    Ge�erli E-posta Adresi Gerekir.";
$lang['mail_subject'] = "Konu Alan  Gereklidir.";
$lang['mail_success'] = "E-posta 2c Ba��ar�yla%g�nder";
$lang['mail_error'] = "Oops%2c E-posta 2c%g�ndermeyecek";
