<?php

$lang['panel_title'] = "B�l�m";
$lang['add_title'] = "Eklemek B�l�m";
$lang['slno'] = "#";
$lang['section_name'] = "B�l�m";
$lang['section_category'] = "Kategori";
$lang['section_capacity'] = "Kapasite";
$lang['section_classes'] = "S�n�f";
$lang['section_department'] = "B�l�m";
$lang['section_teacher_name'] = "Ö��retmen İsim";
$lang['section_note'] = "Not";
$lang['action'] = "Eylem";
$lang['section_select_class'] = "Se�in S�n�f";
$lang['section_select_department'] = "Se�in B�l�m�";
$lang['section_select_teacher'] = "Se�in Ö��retmen";
$lang['view'] = "G�r�n�m";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['add_section'] = "B�l�m Ekle";
$lang['update_section'] = "G�ncelleme B�l�m";
