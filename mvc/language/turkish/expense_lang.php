<?php

$lang['panel_title'] = "Gider";
$lang['add_title'] = "Maliyetler";
$lang['slno'] = "#";
$lang['expense_expense'] = "Ad�";
$lang['expense_date'] = "Tarih";
$lang['expense_amount'] = "Miktar";
$lang['expense_note'] = "Not";
$lang['expense_uname'] = "Kullan�c�";
$lang['expense_total'] = "Toplam";
$lang['expense_file'] = "Dosya";
$lang['expense_file_browse'] = "Dosyas� G�zat";
$lang['expense_clear'] = "A��k";
$lang['expense_download'] = "İndir";
$lang['action'] = "Eylem";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['add_expense'] = "Masraf Ekle";
$lang['update_expense'] = "G�ncelleme Gider";
