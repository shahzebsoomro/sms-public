<?php

$lang['panel_title'] = "Medya";
$lang['add_title'] = "Klas�r Olu��turun";
$lang['slno'] = "#";
$lang['media_title'] = "Ba��l�k";
$lang['media_date'] = "Tarih";
$lang['media_uploadedby'] = "Tarih";
$lang['media_clear'] = "A��k";
$lang['media_file_browse'] = "Dosyas� G�zat";
$lang['action'] = "Eylem";
$lang['view'] = "G�r�n�m";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['add_class'] = "Medya Ekle";
$lang['update_class'] = "G�ncelleme Medya";
$lang['file'] = "Medya";
$lang['upload_file'] = "Upload Medya";
$lang['folder_name'] = "Klas�r Isim";
$lang['share'] = "Payla��mak";
$lang['share_with'] = "Pay";
$lang['select_class'] = "Se�in S�n�f";
$lang['all_class'] = "T�m S�n�f";
$lang['public'] = "Kamu";
$lang['class'] = "S�n�f";
