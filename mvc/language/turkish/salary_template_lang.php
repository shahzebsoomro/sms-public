<?php

$lang['panel_title'] = "Maa�� Şablon";
$lang['add_title'] = "Eklemek Maa�� Bir ��ablon";
$lang['slno'] = "#";
$lang['salary_template_salary_grades'] = "Maa�� Notlar�";
$lang['salary_template_basic_salary'] = "Temel Maa��";
$lang['salary_template_overtime_rate'] = "Ba���na Fazla Mesai Oran� %28 %29";
$lang['salary_template_overtime_rate_not_hour'] = "Fazla Mesai Oran�";
$lang['salary_template_allowances'] = "Ödenekleri";
$lang['salary_template_deductions'] = "T�mdengelim";
$lang['salary_template_allowances_label'] = "Ödenek Etiket Girin";
$lang['salary_template_allowances_value'] = "Ödenek De��er Girin";
$lang['salary_template_deductions_label'] = "Girin İndirim Etiketi";
$lang['salary_template_deductions_value'] = "Girin Kesintiler De��eri";
$lang['salary_template_total_salary_details'] = "Toplam Maa�� Detaylar";
$lang['salary_template_gross_salary'] = "Br�t Maa��";
$lang['salary_template_total_deduction'] = "Toplam İndirimi";
$lang['salary_template_net_salary'] = "Net Maa��";
$lang['salary_template_allowances_val'] = "ödenek De��er";
$lang['salary_template_deductions_val'] = "Kesintiler De��eri";
$lang['add_salary_template'] = "Maa�� Şablon Ekle";
$lang['update_salary_template'] = "G�ncelleme Maa�� Şablon";
$lang['action'] = "Eylem";
$lang['view'] = "G�r�n�m";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
