<?php

$lang['panel_title'] = "Varl�k";
$lang['add_title'] = "Bir Varl�k Eklemek";
$lang['slno'] = "#";
$lang['asset_serial'] = "Seri";
$lang['asset_description'] = "Ba��l�k";
$lang['asset_attachment'] = "Eki";
$lang['asset_status'] = "Durumu";
$lang['asset_categoryID'] = "Kategori";
$lang['asset_locationID'] = "Konum";
$lang['asset_condition'] = "Durumu";
$lang['asset_select_status'] = "Se�in Durum";
$lang['asset_select_condition'] = "Se�in Durum";
$lang['asset_select_category'] = "Se�in Kategori";
$lang['asset_select_location'] = "Se�in Konum";
$lang['asset_status_checked_out'] = "Kontrol";
$lang['asset_create_date'] = "Tarih Olu��turmak";
$lang['asset_status_checked_in'] = "Depolama";
$lang['asset_condition_new'] = "Yeni";
$lang['asset_condition_used'] = "Kullan�lan";
$lang['asset_add'] = "Ekle";
$lang['action'] = "Eylem";
$lang['asset_file_browse'] = "Dosyas� G�zat";
$lang['asset_clear'] = "A��k";
$lang['view'] = "G�r�n�m";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['print'] = "Bask�";
$lang['pdf_preview'] = "Pdf Önizleme";
$lang["mail"] = "Posta  Pdf G�nder";
$lang['add_asset'] = "Varl�k Ekle";
$lang['update_asset'] = "G�ncelleme Varl�k";
$lang['to'] = "İ�in";
$lang['subject'] = "Konu";
$lang['message'] = "Mesaj";
$lang['send'] = "G�nder";
$lang['mail_to'] = "Alan   Gereklidir.";
$lang['mail_valid'] = "Alan    Ge�erli E-posta Adresi Gerekir.";
$lang['mail_subject'] = "Konu Alan  Gereklidir.";
$lang['mail_success'] = "E-posta 2c Ba��ar�yla%g�nder";
$lang['mail_error'] = "Oops%2c E-posta 2c%g�ndermeyecek";
