<?php

$lang['panel_title'] = "Pansiyon";
$lang['add_title'] = "Hostel Ekle";
$lang['slno'] = "#";
$lang['hostel_name'] = "Ad�";
$lang['hostel_htype'] = "Yaz�n";
$lang['hostel_address'] = "Adres";
$lang['hostel_note'] = "Not";
$lang['select_hostel_type'] = "Se�in Tip";
$lang['hostel_boys'] = "Çocuklar";
$lang['hostel_girls'] = "K�zlar";
$lang['hostel_combine'] = "Birle��tirmek";
$lang['action'] = "Eylem";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['add_hostel'] = "Hostel Ekle";
$lang['update_hostel'] = "G�ncelleme Hostel";
