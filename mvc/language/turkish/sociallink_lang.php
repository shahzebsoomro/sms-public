<?php

$lang['panel_title'] = "Sosyal Link";
$lang['add_title'] = "Eklemek Sosyal Ba��lant�";
$lang['slno'] = "#";
$lang['sociallink_title'] = "Ba��l�k";
$lang['sociallink_add'] = "Ekle";
$lang['action'] = "Eylem";
$lang['sociallink_role_select'] = "Se�in Rol";
$lang['sociallink_role'] = "Rol";
$lang['sociallink_user_select'] = "Se�in Kullan�c�";
$lang['sociallink_photo'] = "Foto��raf";
$lang['sociallink_user'] = "Kullan�c�";
$lang['sociallink_facebook'] = "Facebook";
$lang['sociallink_twitter'] = "Heyecan";
$lang['sociallink_linkedin'] = "Linked�n";
$lang['sociallink_googleplus'] = "Google Plus";
$lang['sociallink_userroleID'] = "Kullan�c� Rol";
$lang['sociallink_userID'] = "Kullan�c�";
$lang['view'] = "G�r�n�m";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['print'] = "Bask�";
$lang['pdf_preview'] = "Pdf Önizleme";
$lang["mail"] = "Posta  Pdf G�nder";
$lang['add_sociallink'] = "Sosyal Ba��lant�s�n� Ekleyin";
$lang['update_sociallink'] = "G�ncelleme Sosyal Link";
