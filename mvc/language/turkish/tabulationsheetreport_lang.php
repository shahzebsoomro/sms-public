<?php

$lang['panel_title'] = "Tablolama Formu Rapor";
$lang['tabulationsheetreport_please_select'] = "L�tfen Se�in";
$lang['tabulationsheetreport_report_for'] = "Rapor İ�in";
$lang['tabulationsheetreport_class'] = "S�n�f";
$lang['tabulationsheetreport_section'] = "B�l�m";
$lang['tabulationsheetreport_student'] = "Ö��renci";
$lang['tabulationsheetreport_exam'] = "S�nav";
$lang['tabulationsheetreport_tabulationsheet'] = "Tablolama Sayfas�";
$lang['tabulationsheetreport_all_class'] = "T�m S�n�f";
$lang['tabulationsheetreport_all_section'] = "T�m B�l�m";
$lang['tabulationsheetreport_name'] = "Ad�";
$lang['tabulationsheetreport_roll'] = "Rulo";
$lang['tabulationsheetreport_gpa'] = "Gpa";
$lang['tabulationsheetreport_total'] = "Toplam";
$lang['tabulationsheetreport_print'] = "Bask�";
$lang['tabulationsheetreport_submit'] = "Rapor Almak";
$lang['tabulationsheetreport_pdf_preview'] = "Pdf Önizleme";
$lang['tabulationsheetreport_send_pdf_to_mail'] = "Posta  Pdf G�nder";
$lang['tabulationsheetreport_hotline'] = "Hatt�";
$lang['tabulationsheetreport_to'] = "İ�in";
$lang['tabulationsheetreport_subject'] = "Konu";
$lang['tabulationsheetreport_message'] = "Mesaj";
$lang['tabulationsheetreport_close'] = "Yak�n";
$lang['tabulationsheetreport_send'] = "G�nder";
$lang['tabulationsheetreport_mail_to'] = "Alan   Gereklidir.";
$lang['tabulationsheetreport_mail_valid'] = "Alan    Ge�erli E-posta Adresi Gerekir.";
$lang['tabulationsheetreport_mail_subject'] = "Konu Alan  Gereklidir.";
$lang['mail_success'] = "E-posta 2c Ba��ar�yla%g�nder";
$lang['mail_error'] = "Oops%2c E-posta 2c%g�ndermeyecek";
$lang['tabulationsheetreport_data_not_found'] = "Yok%27t Herhangi Veri .";
$lang['tabulationsheetreport_permission'] = "İzin  De��il";
$lang['tabulationsheetreport_permissionmethod'] = "Y�ntem  İzin Verilmiyor";
