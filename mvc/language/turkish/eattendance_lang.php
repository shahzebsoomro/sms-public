<?php

$lang['panel_title'] = "S�nav Kat�l�m";
$lang['add_title'] = "S�nav Kat�l�m Ekle";
$lang['slno'] = "#";
$lang['eattendance_photo'] = "Foto��raf";
$lang['eattendance_name'] = "Ad�";
$lang['eattendance_email'] = "E-posta";
$lang['eattendance_roll'] = "Rulo";
$lang['eattendance_phone'] = "Telefon";
$lang['eattendance_attendance'] = "Kat�l�m";
$lang['eattendance_section'] = "B�l�m";
$lang['eattendance_exam'] = "S�nav";
$lang['eattendance_classes'] = "S�n�f";
$lang['eattendance_subject'] = "Konu";
$lang['eattendance_status'] = "Durumu";
$lang['eattendance_student'] = "Ö��renci";
$lang['eattendance_all_students'] = "T�m Ö��renciler";
$lang['eattendance_present'] = "Mevcut";
$lang['eattendance_absent'] = "Yok";
$lang['eattendance_details'] = "S�nav Kat�l�m Ayr�nt�lar";
$lang['eattendance_select_exam'] = "Se�in S�nav";
$lang['eattendance_select_classes'] = "Se�in S�n�f";
$lang['eattendance_select_subject'] = "Se�in Konu";
$lang['eattendance_select_section'] = "Se�in B�l�m";
$lang['eattendance_select_student'] = "Se�in Ö��renci";
$lang['action'] = "Eylem";
$lang['add_attendance'] = "Kat�l�m";
$lang['add_all_attendance'] = "T�m Kat�l�m Ekle";
$lang['view_attendance'] = "G�r�n�m Kat�l�m";
