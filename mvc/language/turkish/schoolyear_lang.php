<?php

$lang['panel_title'] = "Akademik Y�l";
$lang['add_title'] = "Akademik Bir Y�l Ekleyin";
$lang['slno'] = "#";
$lang['schoolyear_schoolyear'] = "Y�l";
$lang['schoolyear_schoolyeartitle'] = "Y�l Ba��l�k";
$lang['schoolyear_semestertitle'] = "D�nem Ba��l�k";
$lang['schoolyear_startingdate'] = "Ba��lang�� Tarihi";
$lang['schoolyear_endingdate']   = "Biti�� Tarihi";
$lang['schoolyear_semestercode'] = "D�nem Kodu";
$lang['schoolyear_all'] = "T�m";
$lang['schoolyear_classbase'] = "S�n�f Baz";
$lang['schoolyear_semesterbase'] = "D�nem Baz";
$lang['action'] = "Eylem";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['add_schoolyear']    = "Eklemek Y�l";
$lang['update_schoolyear'] = "G�ncelleme Y�l";
