<?php

$lang['panel_title'] = "Faaliyetler";
$lang['add_title'] = "Aktiviteler Ekleyin";
$lang['slno'] = "#";
$lang['activities_title'] = "Ba��l�k";
$lang['activities_description'] = "A��klama";
$lang['activities_students'] = "Ö��renciler";
$lang['activities_shared_publicly'] = "Payla���lan Halka";
$lang['file_browse'] = "G�z Dosya";
$lang['attachment'] = "Eki";
$lang['clear'] = "A��k";
$lang['activities_time_frame'] = "Zaman Çer�eve";
$lang['activities_time_from'] = "Zaman";
$lang['activities_time_to'] = "Zaman";
$lang['activities_time_at'] = "Zaman";
$lang['students'] = "Ö��renci Liste";
$lang['activities_create_date'] = "Tarih Olu��turmak";
$lang['action'] = "Eylem";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['add_activities'] = "Aktiviteler Ekleyin";
$lang['update_activities'] = "G�ncelleme Faaliyetleri";
