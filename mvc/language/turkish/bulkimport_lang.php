<?php

$lang['panel_title'] = "Al";
$lang['add_title'] = "Eklemek Alma";
$lang['slno'] = "#";
$lang['import_title'] = "Ba��l�k";
$lang['import_import'] = "Al";
$lang['import_date'] = "Tarih";
$lang['bulkimport_teacher'] = "Ö��retmen Ekle";
$lang['bulkimport_student'] = "Ö��renci Ekle";
$lang['bulkimport_parent'] = "Üst Ekleyin";
$lang['bulkimport_user'] = "Kullan�c� Ekle";
$lang['bulkimport_book'] = "Kitap Ekle";
$lang['bulkimport_submit'] = "Al";
$lang['add_class'] = "Al Ekle";
$lang['upload_file'] = "İthalat Dosya";
$lang['import_file_type_error'] = "Ge�ersiz Dosya";
$lang['import_error'] = "Oops%2c Veriler Ithal De��il%2c L�tfen  Tekrar Deneyin .";
$lang['import_success'] = "Veri Ba��ar�l� Eklendi";
$lang['bulkimport_sample'] = "İndirme Örnek";
