<?php

$lang['panel_title'] = "Rutin Rapor";
$lang['routinereport_please_select'] = "L�tfen Se�in";
$lang['routinereport_routine_for'] = "Rutin";
$lang['routinereport_teacher'] = "Ö��retmen";
$lang['routinereport_class'] = "S�n�f";
$lang['routinereport_section'] = "B�l�m";
$lang['routinereport_routine'] = "Rutin";
$lang['routinereport_student'] = "Ö��renci";
$lang['routinereport_name'] = "Ad�";
$lang['routinereport_report_for'] = "Rapor İ�in";
$lang['routinereport_room'] = "Oda Hay�r";
$lang['routinereport_day'] = "G�n";
$lang['routinereport_period'] = "D�nem";
$lang['routinereport_designation'] = "Atama";
$lang['routinereport_subject'] = "Konu";
$lang['routinereport_data_not_found'] = "Yok%27t Herhangi Veri .";
$lang['sunday'] = "Pazar";
$lang['monday'] = "Pazartes�";
$lang['tuesday'] = "Sali";
$lang['wednesday'] = "Çar�amba";
$lang['thursday'] = "Per�embe";
$lang['friday'] = "Cuma";
$lang['saturday'] = "Cumartes�";
$lang['routinereport_print'] = "Bask�";
$lang['routinereport_submit'] = "Rapor Almak";
$lang['routinereport_report_for'] = "Rapor İ�in";
$lang['routinereport_holiday'] = "Tatil";
$lang['routinereport_print'] = "Bask�";
$lang['routinereport_pdf_preview'] = "Pdf Önizleme";
$lang['routinereport_xml'] = "Xml";
$lang['routinereport_mail'] = "Posta  Pdf G�nder";
$lang['print'] = "Bask�";
$lang['routinereport_hotline'] = "Hatt�";
$lang['routinereport_to'] = "İ�in";
$lang['routinereport_subject'] = "Konu";
$lang['routinereport_message'] = "Mesaj";
$lang['routinereport_close'] = "Yak�n";
$lang['routinereport_send'] = "G�nder";
$lang['routinereport_mail_to'] = "Alan   Gereklidir.";
$lang['routinereport_mail_valid'] = "Alan    Ge�erli E-posta Adresi Gerekir.";
$lang['routinereport_mail_subject'] = "Konu Alan  Gereklidir.";
$lang['mail_success'] = "E-posta 2c Ba��ar�yla%g�nder";
$lang['mail_error'] = "Oops%2c E-posta 2c%g�ndermeyecek";
$lang['routinereport_data_not_found'] = "Veri De��il Buldu";
$lang['routinereport_section_not_found'] = "B�l�m Bulunamad�";
$lang['routinereport_class_not_found'] = "S�n�f De��il Buldu";
$lang['routinereport_examid_not_found'] = "S�nav Id De��il Buldu";
$lang['routinereport_permission'] = "İzin  De��il";
$lang['routinereport_permissionmethod'] = "Y�ntem  İzin Verilmiyor";
$lang['routinereport_class_or_section_not_found'] = "S�n�f Veya B�l�m De��il Buldu";
$lang['routinereport_teacher_not_found'] = "Ö��retmen De��il Buldu";
