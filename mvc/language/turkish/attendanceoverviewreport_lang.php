<?php

$lang['panel_title'] = "Kat�l�m Genel Rapor";
$lang['attendanceoverviewreport_type'] = "Kat�l�m Genel Bak���";
$lang['attendanceoverviewreport_reportfor']     = "Rapor İ�in";
$lang['attendanceoverviewreport_please_select'] = "L�tfen Se�in";
$lang['attendanceoverviewreport_teacher'] = "Ö��retmen";
$lang['attendanceoverviewreport_student'] = "Ö��renci";
$lang['attendanceoverviewreport_user']    = "Kullan�c�";
$lang['attendanceoverviewreport_section'] = "B�l�m";
$lang['attendanceoverviewreport_class']   = "S�n�f";
$lang['attendanceoverviewreport_subject'] = "Konu";
$lang['attendanceoverviewreport_month']   = "Ay";
$lang['attendanceoverviewreport_date']    = "Tarih";
$lang['attendanceoverviewreport_name']    = "Ad�";
$lang['attendanceoverviewreport_count']    = "Say�s�";
$lang['attendanceoverviewreport_roll']    = "Rulo";
$lang['attendanceoverviewreport_1']  = "Bir";
$lang['attendanceoverviewreport_2']  = "İki";
$lang['attendanceoverviewreport_3']  = "Ü�";
$lang['attendanceoverviewreport_4']  = "D�rt";
$lang['attendanceoverviewreport_5']  = "Be��";
$lang['attendanceoverviewreport_6']  = "Alt�";
$lang['attendanceoverviewreport_7']  = "Yedi";
$lang['attendanceoverviewreport_8']  = "Sekiz";
$lang['attendanceoverviewreport_9']  = "Dokuz";
$lang['attendanceoverviewreport_10'] = "On";
$lang['attendanceoverviewreport_11'] = "On Bir";
$lang['attendanceoverviewreport_12'] = "On Iki";
$lang['attendanceoverviewreport_13'] = "On ü�";
$lang['attendanceoverviewreport_14'] = "On D�rt";
$lang['attendanceoverviewreport_15'] = "On Be��";
$lang['attendanceoverviewreport_16'] = "On Alt�";
$lang['attendanceoverviewreport_17'] = "On Yedi";
$lang['attendanceoverviewreport_18'] = "On Sekiz";
$lang['attendanceoverviewreport_19'] = "On Dokuz";
$lang['attendanceoverviewreport_20'] = "Yirmi";
$lang['attendanceoverviewreport_21'] = "Yirmi Bir";
$lang['attendanceoverviewreport_22'] = "Yirmi Iki";
$lang['attendanceoverviewreport_23'] = "Yirmi ü�";
$lang['attendanceoverviewreport_24'] = "Yirmi D�rt";
$lang['attendanceoverviewreport_25'] = "Yirmi Be��";
$lang['attendanceoverviewreport_26'] = "Yirmi Alt�";
$lang['attendanceoverviewreport_27'] = "Yirmi Yedi";
$lang['attendanceoverviewreport_28'] = "Yirmi Sekiz";
$lang['attendanceoverviewreport_29'] = "Yirmi Dokuz";
$lang['attendanceoverviewreport_30'] = "Otuz";
$lang['attendanceoverviewreport_31'] = "Otuz Bir";
$lang['attendanceoverviewreport_slno'] = "#";
$lang['attendanceoverviewreport_january']  = "Ocak";
$lang['attendanceoverviewreport_february'] = "Şubat";
$lang['attendanceoverviewreport_march']    = "Mart";
$lang['attendanceoverviewreport_april']    = "Nisan";
$lang['attendanceoverviewreport_may']      = "Olabilir";
$lang['attendanceoverviewreport_june']     = "Haziran";
$lang['attendanceoverviewreport_july']     = "Temmuz";
$lang['attendanceoverviewreport_august']   = "A��ustos";
$lang['attendanceoverviewreport_september']= "Eyl�l";
$lang['attendanceoverviewreport_october']  = "Ekim";
$lang['attendanceoverviewreport_november'] = "Kas�m";
$lang['attendanceoverviewreport_december'] = "Aral�k";
$lang['attendanceoverviewreport_select_all_section'] = "T�m B�l�m";
$lang['attendanceoverviewreport_present'] = "Mevcut";
$lang['attendanceoverviewreport_late_present_with_excuse'] = "Bahane İle Ge� Hediye";
$lang['attendanceoverviewreport_late_present'] = "Ge� Hediye";
$lang['attendanceoverviewreport_absent'] = "Yok";
$lang['attendanceoverviewreport_submit'] = "Rapor Almak";
$lang['attendanceoverviewreport_print'] = "Bask�";
$lang['attendanceoverviewreport_pdf_preview'] = "Pdf Önizleme";
$lang['attendanceoverviewreport_xml'] = "Xml";
$lang['attendanceoverviewreport_mail'] = "Posta  Pdf G�nder";
$lang['attendanceoverviewreport_to'] = "İ�in";
$lang['attendanceoverviewreport_subject'] = "Konu";
$lang['attendanceoverviewreport_message'] = "Mesaj";
$lang['attendanceoverviewreport_close'] = "Yak�n";
$lang['attendanceoverviewreport_send'] = "G�nder";
$lang['attendanceoverviewreport_slno'] = "#";
$lang['attendanceoverviewreport_hotline'] = "Hatt�";
$lang['attendanceoverviewreport_h'] = "Sa";
$lang['attendanceoverviewreport_w'] = "G";
$lang['attendanceoverviewreport_p'] = "P";
$lang['attendanceoverviewreport_le'] = "Le";
$lang['attendanceoverviewreport_l'] = "L";
$lang['attendanceoverviewreport_a'] = "Bir";
$lang['attendanceoverviewreport_mail_to'] = "Alan   Gereklidir.";
$lang['attendanceoverviewreport_mail_valid'] = "Alan    Ge�erli E-posta Adresi Gerekir.";
$lang['attendanceoverviewreport_mail_subject'] = "Konu Alan  Gereklidir.";
$lang['mail_success'] = "E-posta 2c Ba��ar�yla%g�nder";
$lang['mail_error'] = "Oops%2c E-posta 2c%g�ndermeyecek";
$lang['attendanceoverviewreport_data_not_found'] = "Veri De��il Buldu";
$lang['attendanceoverviewreport_permission'] = "İzin  De��il";
$lang['attendanceoverviewreport_permissionmethod'] = "Y�ntem  İzin Verilmiyor";
