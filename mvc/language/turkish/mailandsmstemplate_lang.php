<?php

$lang['panel_title'] = "Posta %2f Sms Şablon";
$lang['add_title'] = "Ekle  ��ablon";
$lang['slno'] = "#";
$lang['mailandsmstemplate_name'] = "Ad�";
$lang['mailandsmstemplate_user'] = "Kullan�c�";
$lang['mailandsmstemplate_tags'] = "Etiketler";
$lang['mailandsmstemplate_template'] = "Şablon";
$lang['mailandsmstemplate_select_user'] = "Se�in Kullan�c�";
$lang['mailandsmstemplate_student'] = "Ö��renci";
$lang['mailandsmstemplate_parents'] = "Ebeveynler";
$lang['mailandsmstemplate_teacher'] = "Ö��retmen";
$lang['mailandsmstemplate_accountant'] = "Muhasebeci";
$lang['mailandsmstemplate_librarian'] = "K�t�phaneci";
$lang['mailandsmstemplate_type'] = "Yaz�n";
$lang['mailandsmstemplate_select_type'] = "Se�in Tip";
$lang['mailandsmstemplate_email'] = "E-posta";
$lang['mailandsmstemplate_sms'] = "Sms";
$lang['mailandsmstemplate_select_tag'] = "Se�in Etiket";
$lang['action'] = "Eylem";
$lang['view'] = "G�r�n�m";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['add_template'] = "Şablon Ekle";
$lang['update_template'] = "G�ncelleme Şablon";
