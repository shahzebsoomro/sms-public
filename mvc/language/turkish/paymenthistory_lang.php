<?php

$lang['panel_title'] = "Ödeme Tarih";
$lang['add_title'] = "Add A S�n�f";
$lang['slno'] = "#";
$lang['paymenthistory_student'] = "Ö��renci";
$lang['paymenthistory_classes'] = "S�n�f";
$lang['paymenthistory_feetype'] = "Ücret Tip";
$lang['paymenthistory_method'] = "Y�ntem";
$lang['paymenthistory_amount'] = "Miktar";
$lang['paymenthistory_date'] = "Tarih";
$lang['paymenthistory_select_paymentmethod'] = "Se�in Ödeme Y�ntemi";
$lang['paymenthistory_select_student'] = "Se�in Ö��renci";
$lang['paymenthistory_amount'] = "Miktar";
$lang['paymenthistory_paymentmethod'] = "Ödeme Y�ntemi";
$lang['paymenthistory_payment_by'] = "Ödeme";
$lang['action'] = "Eylem";
$lang['view'] = "G�r�n�m";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['Cash'] = "Nakit";
$lang['Cheque'] = "Çek";
$lang['Paypal'] = "Paypal";
$lang['Stripe'] = "Şerit";
$lang['PayUmoney'] = "Payumoney";
$lang['update_payment'] = "G�ncelleme Ödeme";
