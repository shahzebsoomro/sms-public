<?php

$lang['panel_title'] = "Ö��renci Kat�l�m";
$lang['panel_title2'] = "Kat�l�m";
$lang['add_title'] = "Ö��renci Kat�l�m Ekle";
$lang['slno'] = "#";
$lang['attendance_classes'] = "S�n�f";
$lang['attendance_section'] = "B�l�m";
$lang['attendance_subject'] = "Konu";
$lang['attendance_student'] = "Ö��renci";
$lang['attendance_studentid'] = "Ö��renci Id";
$lang['attendance_date'] = "Tarih";
$lang['attendance_photo'] = "Foto��raf";
$lang['attendance_name'] = "Ad�";
$lang['attendance_section'] = "B�l�m";
$lang['attendance_roll'] = "Rulo";
$lang['attendance_phone'] = "Telefon";
$lang['attendance_dob'] = "Tarihi Do��um";
$lang['attendance_sex'] = "Cinsiyet";
$lang['attendance_religion'] = "Din";
$lang['attendance_email'] = "E-posta";
$lang['attendance_address'] = "Adres";
$lang['attendance_username'] = "Kullan�c� Ad�";
$lang['attendance_all_students'] = "T�m Ö��renciler";
$lang['attendance_details'] = "Kat�l�m Ayr�nt�lar";
$lang['attendance_day'] = "G�n";
$lang['attendance_registerNO'] = "Kay�t Hayir";
$lang['attendance_bloodgroup'] = "Kan Grup";
$lang['attendance_state'] = "Devlet";
$lang['attendance_country'] = "Ülke";
$lang['attendance_studentgroup'] = "Grup";
$lang['attendance_optionalsubject'] = "İste��e Ba��l� Konu";
$lang['attendance_extracurricularactivities'] = "Ek Ders Faaliyetleri";
$lang['attendance_remarks'] = "Uyar�lar";
$lang['attendance_attendance'] = "Kat�l�m";
$lang['attendance_presentandabsent'] = "Mevcut%2fabsent";
$lang['attendance_late'] = "Ge�";
$lang['attendance_select_classes'] = "Se�in S�n�f";
$lang['attendance_select_section'] = "Se�in B�l�m";
$lang['attendance_select_subject'] = "Se�in Konu";
$lang['attendance_select_student'] = "Se�in Ö��renci";
$lang['personal_information'] = "Ki��isel Bilgi";
$lang['attendance_information'] = "Kat�l�m Bilgi";
$lang['action'] = "Eylem";
$lang['view'] = "G�r�n�m";
$lang['pdf_preview'] = "Pdf Önizleme";
$lang['print'] = "Bask�";
$lang["mail"] = "Posta  Pdf G�nder";
$lang['add_attendance'] = "Kat�l�m";
$lang['add_all_attendance'] = "T�m Kat�l�m Ekle";
$lang['add_all_attendance_late'] = "T�m Ge� Kat�l�m  Ekleyin";
$lang['attendance_jan'] = "Ocak";
$lang['attendance_feb'] = "Şubat";
$lang['attendance_mar'] = "Mart";
$lang['attendance_apr'] = "Nisan";
$lang['attendance_may'] = "Olabilir";
$lang['attendance_june'] = "Haziran";
$lang['attendance_jul'] = "Temmuz";
$lang['attendance_aug'] = "A��ustos";
$lang['attendance_sep'] = "Eyl�l";
$lang['attendance_oct'] = "Ekim";
$lang['attendance_nov'] = "Kas�m";
$lang['attendance_dec'] = "Aral�k";
$lang['attendance_1'] = "Bir";
$lang['attendance_2'] = "İki";
$lang['attendance_3'] = "Ü�";
$lang['attendance_4'] = "D�rt";
$lang['attendance_5'] = "Be��";
$lang['attendance_6'] = "Alt�";
$lang['attendance_7'] = "Yedi";
$lang['attendance_8'] = "Sekiz";
$lang['attendance_9'] = "Dokuz";
$lang['attendance_10'] = "On";
$lang['attendance_11'] = "On Bir";
$lang['attendance_12'] = "On Iki";
$lang['attendance_13'] = "On ü�";
$lang['attendance_14'] = "On D�rt";
$lang['attendance_15'] = "On Be��";
$lang['attendance_16'] = "On Alt�";
$lang['attendance_17'] = "On Yedi";
$lang['attendance_18'] = "On Sekiz";
$lang['attendance_19'] = "On Dokuz";
$lang['attendance_20'] = "Yirmi";
$lang['attendance_21'] = "Yirmi Bir";
$lang['attendance_22'] = "Yirmi Iki";
$lang['attendance_23'] = "Yirmi ü�";
$lang['attendance_24'] = "Yirmi D�rt";
$lang['attendance_25'] = "Yirmi Be��";
$lang['attendance_26'] = "Yirmi Alt�";
$lang['attendance_27'] = "Yirmi Yedi";
$lang['attendance_28'] = "Yirmi Sekiz";
$lang['attendance_29'] = "Yirmi Dokuz";
$lang['attendance_30'] = "Otuz";
$lang['attendance_31'] = "Otuz Bir";
$lang['to'] = "İ�in";
$lang['subject'] = "Konu";
$lang['message'] = "Mesaj";
$lang['send'] = "G�nder";
$lang['mail_to'] = "Alan   Gereklidir.";
$lang['mail_valid'] = "Alan    Ge�erli E-posta Adresi Gerekir.";
$lang['mail_subject'] = "Konu Alan  Gereklidir.";
$lang['mail_success'] = "E-posta 2c Ba��ar�yla%g�nder";
$lang['mail_error'] = "Oops%2c E-posta 2c%g�ndermeyecek";
$lang['sattendance_submit'] = "G�nder";
$lang['sattendance_present'] = "Hediye";
$lang['sattendance_absent'] = "Yok";
$lang['sattendance_late_present'] = "Ge� Hediye";
$lang['sattendance_late_excuse'] = "Öz�r Ge� Hediye";
$lang['sattendance_day']        = "G�n";
$lang['sattendance_classes']    = "S�n�flar";
$lang['sattendance_section']    = "B�l�m";
$lang['sattendance_subject']    = "Konu";
$lang['sattendance_monthyear']  = "Monthyear";
$lang['sattendance_attendance'] = "Kat�l�m";
$lang['sattendance_subject'] = "Konu";
$lang['sattendance_total_mark'] = "Toplam İ��areti";
$lang['sattendance_point'] = "Nokta";
$lang['sattendance_grade'] = "S�n�f";
$lang['sattendance_grade'] = "S�n�f";
$lang['sattendance_not_found'] = "Bulunamad�";
$lang['sattendance_report'] = "Kat�l�m Rapor";
$lang['sattendance_hotline'] = "Hatt�";
$lang['sattendance_total_marks']  = "Toplam İ��aretleri";
$lang['sattendance_average_marks'] = "Ortalama İ��aretleri";
$lang['sattendance_average_grade'] = "Ortalama Notu";
$lang['sattendance_average_point'] = "Ortalama Point";
$lang['sattendance_type'] = "Yaz�n";
$lang['sattendance_to'] = "İ�in";
$lang['sattendance_subject'] = "Konu";
$lang['sattendance_message'] = "Mesaj";
$lang['sattendance_studentID'] = "Ö��renci Id";
$lang['sattendance_classesID'] = "S�n�flar Id";
$lang['sattendance_data_not_found'] = "Yok%27t Herhangi Veri .";
$lang['sattendance_permissionmethod'] = "Y�ntem  İzin Verilmiyor";
$lang['sattendance_permission'] = "İzin  De��il";
$lang['sattendance_student'] = "Ö��renci";
$lang['sattendance_total_holiday'] = "Toplam Tatil";
$lang['sattendance_total_weekenday'] = "Toplam Weekenday";
$lang['sattendance_total_present'] = "Toplam Hediye";
$lang['sattendance_total_latewithexcuse'] = "Bahane İle Toplam Ge�";
$lang['sattendance_total_late'] = "Toplam Ge�";
$lang['sattendance_total_absent'] = "Toplam Devams�zl�k";
