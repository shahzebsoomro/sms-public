<?php

$lang['panel_title'] = "Kitap";
$lang['add_title'] = "Ekle  Bir Kitap";
$lang['slno'] = "#";
$lang['book_name'] = "Ad�";
$lang['book_subject_code'] = "Konu Kodu";
$lang['book_author'] = "Yazar";
$lang['book_price'] = "Fiyat";
$lang['book_quantity'] = "Miktar";
$lang['book_rack_no'] = "Raf Hay�r";
$lang['book_status'] = "Durumu";
$lang['book_available'] = "Mevcut";
$lang['book_unavailable'] = "M�sait";
$lang['action'] = "Eylem";
$lang['view'] = "G�r�n�m";
$lang['edit'] = "D�zenle";
$lang['delete'] = "Sil";
$lang['add_book'] = "Kitap Ekle";
$lang['update_book'] = "G�ncelleme Kitap";
