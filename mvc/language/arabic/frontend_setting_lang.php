<?php

$lang['panel_title'] = "الواجهة إعدادات";
$lang['frontend_setting_facebook'] = "Facebook";
$lang['frontend_setting_twitter'] = "تويتر";
$lang['frontend_setting_linkedin'] = "ينكدين";
$lang['frontend_setting_youtube'] = "يوتيوب";
$lang['frontend_setting_google'] = "Google %2b";
$lang['frontend_setting_frontend_configaration'] = "الواجهة Configaration";
$lang['frontend_setting_social'] = "الاجتماعية";
$lang['frontend_setting_description'] = "الوصف";
$lang['frontend_setting_teacher_email'] = "المعلم البريد الإلكتروني";
$lang['frontend_setting_teacher_phone'] = "المعلم الهاتف";
$lang['frontend_setting_enable'] = "تمكين";
$lang['frontend_setting_disable'] = "تعطيل";
$lang['update_frontend_setting'] = "تحديث الواجهة وضع";
