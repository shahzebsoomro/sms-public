<?php

$lang['panel_title'] = "الكتب الإلكترونية";
$lang['slno'] = "#";
$lang['ebooks_name'] = "اسم";
$lang['ebooks_author'] = "الكاتب";
$lang['ebooks_classes'] = "الدرجة";
$lang['ebooks_authority'] = "السلطة";
$lang['ebooks_private'] = "خاصة";
$lang['ebooks_cover_photo'] = "غلاف صورة";
$lang['ebooks_file'] = "الملف";
$lang['action'] = "العمل";
$lang['ebooks_select_class'] = "حدد فئة";
$lang['ebooks_select_department'] = "حدد قسم";
$lang['ebooks_select_teacher'] = "";
$lang['view'] = "عرض";
$lang['edit'] = "تحرير";
$lang['delete'] = "حذف";
$lang['add_ebooks'] = "إضافة Ebook";
$lang['add_title'] = "إضافة Ebook";
$lang['update_ebooks'] = "تحديث Ebook";
