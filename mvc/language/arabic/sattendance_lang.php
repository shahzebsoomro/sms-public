<?php

$lang['panel_title'] = "الطالب الحضور";
$lang['panel_title2'] = "الحضور";
$lang['add_title'] = "إضافة الطالب الحضور";
$lang['slno'] = "#";
$lang['attendance_classes'] = "الدرجة";
$lang['attendance_section'] = "القسم";
$lang['attendance_subject'] = "الموضوع";
$lang['attendance_student'] = "طالب";
$lang['attendance_studentid'] = "الطالب Id";
$lang['attendance_date'] = "تاريخ";
$lang['attendance_photo'] = "الصورة";
$lang['attendance_name'] = "اسم";
$lang['attendance_section'] = "القسم";
$lang['attendance_roll'] = "لفة";
$lang['attendance_phone'] = "الهاتف";
$lang['attendance_dob'] = "تاريخ  الولادة";
$lang['attendance_sex'] = "الجنس";
$lang['attendance_religion'] = "الدين";
$lang['attendance_email'] = "البريد الإلكتروني";
$lang['attendance_address'] = "عنوان";
$lang['attendance_username'] = "اسم المستخدم";
$lang['attendance_all_students'] = "كل الطلاب";
$lang['attendance_details'] = "الحضور تفاصيل";
$lang['attendance_day'] = "اليوم";
$lang['attendance_registerNO'] = "التسجيل لا";
$lang['attendance_bloodgroup'] = "الدم مجموعة";
$lang['attendance_state'] = "الدولة";
$lang['attendance_country'] = "البلد";
$lang['attendance_studentgroup'] = "المجموعة";
$lang['attendance_optionalsubject'] = "اختياري الموضوع";
$lang['attendance_extracurricularactivities'] = "اضافية المناهج الأنشطة";
$lang['attendance_remarks'] = "ملاحظات";
$lang['attendance_attendance'] = "الحضور";
$lang['attendance_presentandabsent'] = "الحاضر%2fabsent";
$lang['attendance_late'] = "في وقت متأخر";
$lang['attendance_select_classes'] = "حدد فئة";
$lang['attendance_select_section'] = "حدد القسم";
$lang['attendance_select_subject'] = "حدد الموضوع";
$lang['attendance_select_student'] = "حدد طالب";
$lang['personal_information'] = "الشخصية معلومات";
$lang['attendance_information'] = "الحضور معلومات";
$lang['action'] = "العمل";
$lang['view'] = "";
$lang['pdf_preview'] = "Pdf المعاينة";
$lang['print'] = "طباعة";
$lang["mail"] = "إرسال Pdf إلى البريد";
$lang['add_attendance'] = "الحضور";
$lang['add_all_attendance'] = "إضافة كل الحضور";
$lang['add_all_attendance_late'] = "إضافة كل أواخر الحضور";
$lang['attendance_jan'] = "كانون الثاني / يناير";
$lang['attendance_feb'] = "شباط / فبراير";
$lang['attendance_mar'] = "آذار / مارس";
$lang['attendance_apr'] = "نيسان / أبريل";
$lang['attendance_may'] = "قد";
$lang['attendance_june'] = "حزيران / يونيه";
$lang['attendance_jul'] = "تموز / يوليه";
$lang['attendance_aug'] = "آب / أغسطس";
$lang['attendance_sep'] = "أيلول / سبتمبر";
$lang['attendance_oct'] = "تشرين الأول / أكتوبر";
$lang['attendance_nov'] = "تشرين الثاني / نوفمبر";
$lang['attendance_dec'] = "كانون الأول / ديسمبر";
$lang['attendance_1'] = "واحد";
$lang['attendance_2'] = "اثنين";
$lang['attendance_3'] = "ثلاثة";
$lang['attendance_4'] = "أربعة";
$lang['attendance_5'] = "خمسة";
$lang['attendance_6'] = "ستة";
$lang['attendance_7'] = "سبعة";
$lang['attendance_8'] = "ثمانية";
$lang['attendance_9'] = "تسعة";
$lang['attendance_10'] = "عشرة";
$lang['attendance_11'] = "أحد عشر";
$lang['attendance_12'] = "اثني عشر";
$lang['attendance_13'] = "ثلاثة عشر";
$lang['attendance_14'] = "أربعة عشر";
$lang['attendance_15'] = "خمسة عشر";
$lang['attendance_16'] = "ستة عشر";
$lang['attendance_17'] = "سبعة عشر";
$lang['attendance_18'] = "ثمانية عشر";
$lang['attendance_19'] = "تسعة عشر";
$lang['attendance_20'] = "عشرين";
$lang['attendance_21'] = "عشرين واحد";
$lang['attendance_22'] = "اثنين وعشرين";
$lang['attendance_23'] = "ثلاثة وعشرون";
$lang['attendance_24'] = "أربع وعشرين";
$lang['attendance_25'] = "خمسة وعشرين";
$lang['attendance_26'] = "ستة وعشرون";
$lang['attendance_27'] = "سبعة وعشرين";
$lang['attendance_28'] = "ثمانية وعشرين";
$lang['attendance_29'] = "تسعة وعشرون";
$lang['attendance_30'] = "ثلاثين";
$lang['attendance_31'] = "واحد وثلاثون";
$lang['to'] = "إلى";
$lang['subject'] = "الموضوع";
$lang['message'] = "الرسالة";
$lang['send'] = "إرسال";
$lang['mail_to'] = "ميدان  مطلوب.";
$lang['mail_valid'] = "ميدان أن تحتوي على A صالح البريد الإلكتروني العنوان.";
$lang['mail_subject'] = "الموضوع ميدان  مطلوب.";
$lang['mail_success'] = "البريد الإلكتروني إرسال بنجاح%2c";
$lang['mail_error'] = "عفوا%2c البريد الإلكتروني لا إرسال%2c";
$lang['sattendance_submit'] = "تقدم";
$lang['sattendance_present'] = "الحاضر";
$lang['sattendance_absent'] = "غائبة";
$lang['sattendance_late_present'] = "في وقت متأخر الحاضر";
$lang['sattendance_late_excuse'] = "في وقت متأخر الحاضر مع عذر";
$lang['sattendance_day']        = "اليوم";
$lang['sattendance_classes']    = "دروس";
$lang['sattendance_section']    = "القسم";
$lang['sattendance_subject']    = "الموضوع";
$lang['sattendance_monthyear']  = "Monthyear";
$lang['sattendance_attendance'] = "الحضور";
$lang['sattendance_subject'] = "الموضوع";
$lang['sattendance_total_mark'] = "المجموع علامة";
$lang['sattendance_point'] = "نقطة";
$lang['sattendance_grade'] = "الصف";
$lang['sattendance_grade'] = "الصف";
$lang['sattendance_not_found'] = "لا وجدت";
$lang['sattendance_report'] = "الحضور تقرير";
$lang['sattendance_hotline'] = "الخط الساخن";
$lang['sattendance_total_marks']  = "المجموع علامات";
$lang['sattendance_average_marks'] = "متوسط علامات";
$lang['sattendance_average_grade'] = "متوسط الصف";
$lang['sattendance_average_point'] = "متوسط نقطة";
$lang['sattendance_type'] = "نوع";
$lang['sattendance_to'] = "إلى";
$lang['sattendance_subject'] = "الموضوع";
$lang['sattendance_message'] = "الرسالة";
$lang['sattendance_studentID'] = "الطالب Id";
$lang['sattendance_classesID'] = "دروس Id";
$lang['sattendance_data_not_found'] = "لا%27t  أي البيانات.";
$lang['sattendance_permissionmethod'] = "الطريقة لا سمحت";
$lang['sattendance_permission'] = "إذن لا سمحت";
$lang['sattendance_student'] = "طالب";
$lang['sattendance_total_holiday'] = "المجموع عطلة";
$lang['sattendance_total_weekenday'] = "المجموع Weekenday";
$lang['sattendance_total_present'] = "المجموع الحالي";
$lang['sattendance_total_latewithexcuse'] = "المجموع أواخر مع عذر";
$lang['sattendance_total_late'] = "المجموع وقت متأخر";
$lang['sattendance_total_absent'] = "المجموع غائبة";
