<?php

$lang['panel_title'] = "البريد %2f Sms";
$lang['add_title'] = "إضافة A البريد %2f Sms";
$lang['slno'] = "#";
$lang['mailandsms_usertype'] = "دور";
$lang['mailandsms_users'] = "المستخدمين";
$lang['mailandsms_class'] = "الدرجة";
$lang['mailandsms_schoolyear'] = "المدرسة العام";
$lang['mailandsms_select_usertype'] = "حدد المستخدمين نوع";
$lang['mailandsms_select_users'] = "حدد المستخدمين";
$lang['mailandsms_select_class'] = "حدد فئة";
$lang['mailandsms_select_schoolyear'] = "حدد المدرسة العام";
$lang['mailandsms_select_template'] = "حدد قالب";
$lang['mailandsms_select_send_by'] = "حدد إرسال قبل";
$lang['mailandsms_students'] = "الطلاب";
$lang['mailandsms_parents'] = "الآباء";
$lang['mailandsms_teachers'] = "المعلمين";
$lang['mailandsms_librarians'] = "المكتبات";
$lang['mailandsms_accountants'] = "المحاسبين";
$lang['mailandsms_template'] = "قالب";
$lang['mailandsms_type'] = "نوع";
$lang['mailandsms_email'] = "البريد الإلكتروني";
$lang['mailandsms_sms'] = "Sms";
$lang['mailandsms_getway'] = "إرسال قبل";
$lang['mailandsms_subject'] = "الموضوع";
$lang['mailandsms_message'] = "الرسالة";
$lang['mailandsms_dateandtime'] = "تاريخ  الوقت";
$lang['mailandsms_default'] = "%28default%29";
$lang['mailandsms_reciver_user'] = "رسالة Recevier المستخدم %3a";
$lang['mailandsms_reciver_users'] = "رسالة Recevier المستخدمين %3a";
$lang['mailandsms_not_found'] = "لا وجدت";
$lang['mailandsms_total_mark'] = "المجموع علامة";
$lang['mailandsms_point'] = "نقطة";
$lang['mailandsms_grade'] = "الصف";
$lang['mailandsms_total_marks']  = "";
$lang['mailandsms_average_marks'] = "متوسط علامات";
$lang['mailandsms_average_grade'] = "متوسط الصف";
$lang['mailandsms_average_point'] = "متوسط نقطة";
$lang['mailandsms_clickatell'] = "Clickatell";
$lang['mailandsms_twilio'] = "Twilio";
$lang['mailandsms_bulk'] = "بالجملة";
$lang['mailandsms_msg91'] = "Msg91";
$lang['mailandsms_muthofun'] = "Muthofun";
$lang['action'] = "العمل";
$lang['view'] = "عرض";
$lang['send'] = "إرسال";
$lang['mail_success'] = "البريد الإلكتروني إرسال بنجاح%2c";
$lang['mail_error'] = "عفوا%2c البريد الإلكتروني لا إرسال%2c";
$lang['mail_error_user'] = "عفوا%2c المستخدم قائمة  فارغة%2c";
$lang['mailandsms_notfound_error'] = "المستخدم جرعة لا وجدت.";
