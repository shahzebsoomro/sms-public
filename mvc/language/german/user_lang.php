<?php

$lang['panel_title'] = "Benutzer";
$lang['add_title'] = "Fügen Sie Ein User";
$lang['slno'] = "#";
$lang['user_photo'] = "Foto";
$lang['user_name'] = "Name";
$lang['user_email'] = "E-mail";
$lang['user_dob'] = "Datum Der Geburt";
$lang['user_sex'] = "Geschlecht";
$lang['user_sex_male'] = "Männlich";
$lang['user_sex_female'] = "Weiblich";
$lang['user_religion'] = "Religion";
$lang['user_phone'] = "Telefon";
$lang['user_address'] = "Adresse";
$lang['user_jod'] = "Eintritt Datum";
$lang['user_usertype'] = "Rolle";
$lang['user_username'] = "Benutzername";
$lang['user_password'] = "Passwort";
$lang['user_status'] = "Status";
$lang['user_designation'] = "Bezeichnung";
$lang['user_designation'] = "Bezeichnung";
$lang['user_select_usertype'] = "Wählen Sie Rolle";
$lang['user_file_browse'] = "Datei Durchsuchen";
$lang['user_clear'] = "Klar";
$lang['user_quick_add'] = "Schnell Hinzufügen";
$lang['action'] = "Aktion";
$lang['view'] = "Ansicht";
$lang['edit'] = "Bearbeiten";
$lang['delete'] = "Löschen";
$lang['pdf_preview'] = "Pdf-datei Vorschau";
$lang['idcard'] = "Id Karte";
$lang['print'] = "Drucken";
$lang['download'] = "Download";
$lang["mail"] = "Senden Pdf Zu E-mail";
$lang['personal_information'] = "Persönliche Informationen";
$lang['add_user'] = "Add User";
$lang['update_user'] = "Update User";
$lang['to'] = "Zu";
$lang['subject'] = "Thema";
$lang['message'] = "Nachricht";
$lang['send'] = "Senden";
$lang['mail_to'] = "Feld Ist Notwendig.";
$lang['mail_valid'] = "Feld Muss Enthalten  Gültig E-mail Adresse Ein.";
$lang['mail_subject'] = "Das Thema Field Ist Notwendig.";
$lang['mail_success'] = "E-mail Senden Erfolgreich%2c";
$lang['mail_error'] = "Hoppla%2c E-mail Nicht Senden%2c";
$lang['user_title'] = "Titel";
$lang['user_date'] = "Datum";
$lang['user_add_document'] = "Add Dokument";
$lang['user_document_upload'] = "Dokument Hochladen";
$lang['user_file'] = "Datei";
$lang['user_upload'] = "Hochladen";
$lang['user_title_required'] = "Die Titel Feld Ist Notwendig.";
$lang['user_file_required'] = "Die Datei Field Ist Notwendig.";
$lang['user_profile'] = "Profil";
$lang['user_attendance'] = "Teilnahme";
$lang['user_salary'] = "Gehalt";
$lang['user_payment'] = "Zahlung";
$lang['user_document'] = "Dokument";
$lang['user_1'] = "Eins";
$lang['user_2'] = "Zwei";
$lang['user_3'] = "Drei";
$lang['user_4'] = "Vier";
$lang['user_5'] = "Fünf";
$lang['user_6'] = "Sechs";
$lang['user_7'] = "Sieben";
$lang['user_8'] = "Acht";
$lang['user_9'] = "Neun";
$lang['user_10'] = "Zehn";
$lang['user_11'] = "Elf";
$lang['user_12'] = "Zwölf";
$lang['user_13'] = "Dreizehn";
$lang['user_14'] = "Vierzehn";
$lang['user_15'] = "Fünfzehn";
$lang['user_16'] = "Sechzehn";
$lang['user_17'] = "Siebzehn";
$lang['user_18'] = "Achtzehn";
$lang['user_19'] = "Neunzehn";
$lang['user_20'] = "Zwanzig";
$lang['user_21'] = "Eins Und Zwanzig";
$lang['user_22'] = "Zwei Und Zwanzig";
$lang['user_23'] = "Drei Und Zwanzig";
$lang['user_24'] = "Vier Und Zwanzig";
$lang['user_25'] = "Fünf Und Zwanzig";
$lang['user_26'] = "Sechs Und Zwanzig";
$lang['user_27'] = "Sieben Und Zwanzig";
$lang['user_28'] = "Acht Und Zwanzig";
$lang['user_29'] = "Neun Und Zwanzig";
$lang['user_30'] = "Dreißig";
$lang['user_31'] = "Eins Und Dreißig";
$lang['user_salary_grades'] = "Gehalt Noten";
$lang['user_basic_salary'] = "Basic Gehalt";
$lang['user_overtime_rate'] = "Überstunden Rate %28 Pro Stunde%29";
$lang['user_overtime_rate_not_hour'] = "Überstunden Rate";
$lang['user_allowances'] = "Zertifikate";
$lang['user_deductions'] = "Dllowances";
$lang['user_total_salary_details'] = "Summe Gehalt Details";
$lang['user_gross_salary'] = "Brutto Gehalt";
$lang['user_total_deduction'] = "Insgesamt Abzug";
$lang['user_net_salary'] = "Net Gehalt";
$lang['user_hourly_rate'] = "Hourly Rate";
$lang['user_net_hourly_rate'] = "Net Hourly Rate";
$lang['user_net_salary_hourly'] = "Net Gehalt %28hourly%29";
$lang['user_month'] = "Monat";
$lang['user_payment_amount'] = "Zahlung Betrag";
$lang['user_total'] = "Insgesamt";
$lang['user_to'] = "Zu";
$lang['user_subject'] = "Thema";
$lang['user_message'] = "Nachricht";
$lang['user_userID'] = "Benutzer-id";
$lang['user_data_not_found'] = "Don%27t Haben Alle Daten.";
$lang['user_permissionmethod'] = "Methode Nicht Erlaubt";
$lang['user_permission'] = "Berechtigung Nicht Erlaubt";
$lang['user_type'] = "Geben";
$lang['user_total_holiday'] = "Insgesamt Urlaub";
$lang['user_total_weekenday'] = "Insgesamt Weekenday";
$lang['user_total_present'] = "Insgesamt Vorhanden";
$lang['user_total_latewithexcuse'] = "Insgesamt Ende Mit Entschuldigung";
$lang['user_total_late'] = "Insgesamt Ende";
$lang['user_total_absent'] = "Insgesamt Abwesend";
