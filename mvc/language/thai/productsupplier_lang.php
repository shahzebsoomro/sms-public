<?php

$lang['panel_title'] = "Supplier";
$lang['add_title'] = "เพิ่ม A Supplier";
$lang['slno'] = "#";
$lang['productsupplier_companyname'] = "บริษัท ชื่อ";
$lang['productsupplier_suppliername'] = "Supplier ชื่อ";
$lang['productsupplier_email'] = "อีเมล";
$lang['productsupplier_phone'] = "โทรศัพท์";
$lang['productsupplier_address'] = "ที่อยู่";
$lang['action'] = "การกระทำ";
$lang['edit'] = "แก้ไข";
$lang['delete'] = "ลบ";
$lang['add_supplier'] = "เพิ่ม Supplier";
$lang['update_supplier'] = "ปรับปรุง Supplier";
