<?php

$lang['panel_title'] = "เงินเดือน ต้นแบบ";
$lang['add_title'] = "เพิ่ม A เงินเดือน ต้นแบบ";
$lang['slno'] = "#";
$lang['salary_template_salary_grades'] = "เงินเดือน รอเกรด";
$lang['salary_template_basic_salary'] = "พื้นฐาน เงินเดือน";
$lang['salary_template_overtime_rate'] = "ชื่อโครงการ: อัตราการ %28 ต่อ ชั่วโมง%29";
$lang['salary_template_overtime_rate_not_hour'] = "ชื่อโครงการ: อัตราการ";
$lang['salary_template_allowances'] = "Allowances";
$lang['salary_template_deductions'] = "ผิดหั";
$lang['salary_template_allowances_label'] = "ป้อน Allowances ป้ายชื่อ";
$lang['salary_template_allowances_value'] = "ป้อน Allowances ค่า";
$lang['salary_template_deductions_label'] = "ป้อน Deductions ป้ายชื่อ";
$lang['salary_template_deductions_value'] = "ป้อน Deductions ค่า";
$lang['salary_template_total_salary_details'] = "ทั้งหมด เงินเดือน รายละเอียด";
$lang['salary_template_gross_salary'] = "แย่ เงินเดือน";
$lang['salary_template_total_deduction'] = "ทั้งหมด ผิดหั";
$lang['salary_template_net_salary'] = "เน็ เงินเดือน";
$lang['salary_template_allowances_val'] = "Allowances ค่า";
$lang['salary_template_deductions_val'] = "Deductions ค่า";
$lang['add_salary_template'] = "เพิ่ม เงินเดือน ต้นแบบ";
$lang['update_salary_template'] = "ปรับปรุง เงินเดือน ต้นแบบ";
$lang['action'] = "การกระทำ";
$lang['view'] = "มุมมอง";
$lang['edit'] = "แก้ไข";
$lang['delete'] = "ลบ";
