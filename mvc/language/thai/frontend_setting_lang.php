<?php

$lang['panel_title'] = "Frontend การตั้งค่า";
$lang['frontend_setting_facebook'] = "Facebook";
$lang['frontend_setting_twitter'] = "ทวิตเตอร์";
$lang['frontend_setting_linkedin'] = "Linkedin";
$lang['frontend_setting_youtube'] = "ยอดวิวในยูทูป";
$lang['frontend_setting_google'] = "ของกูเกิ้ล %2b";
$lang['frontend_setting_frontend_configaration'] = "Frontend Configaration";
$lang['frontend_setting_social'] = "สังคม";
$lang['frontend_setting_description'] = "รายละเอียด";
$lang['frontend_setting_teacher_email'] = "ครู ส่งอีเมล";
$lang['frontend_setting_teacher_phone'] = "ครู โทรศัพท์";
$lang['frontend_setting_enable'] = "เปิดใช้งาน";
$lang['frontend_setting_disable'] = "ปิดการใช้งาน";
$lang['update_frontend_setting'] = "ปรับปรุง Frontend การตั้งค่า";
