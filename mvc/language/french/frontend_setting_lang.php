<?php

$lang['panel_title'] = "Frontend Paramètres";
$lang['frontend_setting_facebook'] = "Facebook";
$lang['frontend_setting_twitter'] = "Twitter";
$lang['frontend_setting_linkedin'] = "Linkedin";
$lang['frontend_setting_youtube'] = "Youtube";
$lang['frontend_setting_google'] = "Google %2b";
$lang['frontend_setting_frontend_configaration'] = "Frontend Configaration";
$lang['frontend_setting_social'] = "Sociale";
$lang['frontend_setting_description'] = "Description";
$lang['frontend_setting_teacher_email'] = "Enseignant E-mail";
$lang['frontend_setting_teacher_phone'] = "Professeur De Téléphone";
$lang['frontend_setting_enable'] = "Activer";
$lang['frontend_setting_disable'] = "Désactiver";
$lang['update_frontend_setting'] = "Mise à Jour Frontend Réglage";
