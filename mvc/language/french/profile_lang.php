<?php

$lang['panel_title'] = "Profil";
$lang['slno'] = "#";
$lang['profile_roll'] = "Rouleau";
$lang['profile_email'] = "E-mail";
$lang['profile_dob'] = "Date De La Naissance";
$lang['profile_jod'] = "Adhésion Date";
$lang['profile_sex'] = "Genre";
$lang['profile_religion'] = "La Religion";
$lang['profile_phone'] = "Téléphone";
$lang['profile_address'] = "Adresse";
$lang['profile_bloodgroup'] = "Sang Groupe";
$lang['profile_country'] = "Pays";
$lang['profile_registerNO'] = "S'inscrire Pas De";
$lang['profile_state'] = "État";
$lang['profile_section'] = "Section";
$lang['profile_classes'] = "Classe";
$lang['profile_name'] = "Nom";
$lang['profile_photo'] = "Photo";
$lang['profile_designation'] = "Désignation";
$lang['profile_username'] = "Nom D'utilisateur";
$lang['profile_payment'] = "Paiement";
$lang['profile_date'] = "Date";
$lang['profile_teacher'] = "Enseignant";
$lang['profile_title'] = "Titre";
$lang['profile_select_country'] = "Sélectionnez Pays";
$lang['profile_select_bloodgroup'] = "Sélectionnez Sang Groupe";
$lang['profile_clear'] = "Claire";
$lang['profile_file_browse'] = "Fichier Parcourir";
$lang['profile_sex_male'] = "Mâle";
$lang['profile_sex_female'] = "Femelle";
$lang['update_profile'] = "Mise À Jour Profil";
$lang['profile_guardian'] = "Tuteur Nom";
$lang['profile_guargian_name'] = "Tuteur Nom";
$lang['profile_father_name'] = "Père%27s Nom";
$lang['profile_mother_name'] = "Mère%27s Nom";
$lang['profile_father_profession'] = "Père%27s Profession";
$lang['profile_mother_profession'] = "Mère%27s Profession";
$lang['parent_error'] = "Les Parents Ont Pas été Ajout Encore%2c Vous Ajouter Parents D'informations.";
$lang['personal_information'] = "Personnel D'informations";
$lang['parents_information'] = "Les Parents D'informations";
$lang['profile_profile'] = "Profil";
$lang['profile_attendance'] = "La Fréquentation";
$lang['profile_salary'] = "Salaire";
$lang['profile_routine'] = "La Routine";
$lang['profile_parents'] = "Les Parents";
$lang['profile_mark'] = "Marque";
$lang['profile_invoice'] = "Facture";
$lang['profile_children'] = "Les Enfants";
$lang['profile_document'] = "Document";
$lang['profile_studentgroup'] = "Groupe";
$lang['profile_optionalsubject'] = "Option Sujet";
$lang['profile_remarks'] = "Remarques";
$lang['profile_extracurricularactivities'] = "Extra Scolaires Activités";
$lang['print'] = "Imprimer";
$lang['pdf_preview'] = "Pdf Aperçu";
$lang['mail'] = "Envoyer Pdf À Mail";
$lang['edit'] = "Modifier";
$lang['action'] = "Action";
$lang['download'] = "Télécharger";
$lang['to'] = "Pour";
$lang['subject'] = "Sujet";
$lang['message'] = "Message";
$lang['send'] = "Envoyer";
$lang['mail_to'] = "L' De Terrain Est Requis.";
$lang['mail_valid'] = "L' De Terrain Doit Contient Une Valide E-mail Adresse.";
$lang['mail_subject'] = "Le Sujet Terrain Est Requis.";
$lang['mail_success'] = "E-mail Envoyer Avec Succès%2c";
$lang['mail_error'] = "Oups%2c E-mail Pas Envoyer%2c";
$lang['profile_1'] = "Un";
$lang['profile_2'] = "Deux";
$lang['profile_3'] = "Trois";
$lang['profile_4'] = "Quatre";
$lang['profile_5'] = "Cinq";
$lang['profile_6'] = "Six";
$lang['profile_7'] = "Sept";
$lang['profile_8'] = "Huit";
$lang['profile_9'] = "Neuf";
$lang['profile_10'] = "Dix";
$lang['profile_11'] = "Onze";
$lang['profile_12'] = "Douze";
$lang['profile_13'] = "Treize";
$lang['profile_14'] = "Quatorze";
$lang['profile_15'] = "Quinze";
$lang['profile_16'] = "Seize";
$lang['profile_17'] = "Dix Sept";
$lang['profile_18'] = "Dix Huit";
$lang['profile_19'] = "Dix Neuf";
$lang['profile_20'] = "Vingt";
$lang['profile_21'] = "Vingt Et Un";
$lang['profile_22'] = "Vingt Deux";
$lang['profile_23'] = "Vingt Trois";
$lang['profile_24'] = "Vingt Quatre";
$lang['profile_25'] = "Vingt Cinq";
$lang['profile_26'] = "Vingt Six";
$lang['profile_27'] = "Vingt Sept";
$lang['profile_28'] = "Vingt Huit";
$lang['profile_29'] = "Vingt Neuf";
$lang['profile_30'] = "Trente";
$lang['profile_31'] = "Trente Et Un";
$lang['profile_salary_grades'] = "Salaire Notes";
$lang['profile_basic_salary'] = "De Base Salaire";
$lang['profile_overtime_rate'] = "Les Heures Supplémentaires Taux %28 Par Heure%29";
$lang['profile_overtime_rate_not_hour'] = "Les Heures Supplémentaires Taux";
$lang['profile_allowances'] = "Les Allocations";
$lang['profile_deductions'] = "Dllowances";
$lang['profile_total_salary_details'] = "Total Salaire De Détails";
$lang['profile_gross_salary'] = "Brut Salaire";
$lang['profile_total_deduction'] = "Total Déduction";
$lang['profile_net_salary'] = "Net Salaire";
$lang['profile_hourly_rate'] = "Horaire Taux";
$lang['profile_net_hourly_rate'] = "Net Horaire Taux";
$lang['profile_net_salary_hourly'] = "Net Salaire %28hourly%29";
$lang['profile_month'] = "Mois";
$lang['profile_payment_amount'] = "Paiement Montant";
$lang['profile_total'] = "Total";
$lang['profile_day'] = "Jour";
$lang['profile_period'] = "Période";
$lang['sunday'] = "Dimanche";
$lang['monday'] = "Lundi";
$lang['tuesday'] = "Mardi";
$lang['wednesday'] = "Mercredi";
$lang['thursday'] = "Jeudi";
$lang['friday'] = "Vendredi";
$lang['saturday'] = "Samedi";
$lang['profile_subject'] = "Sujet";
$lang['profile_class'] = "Classe";
$lang['profile_section'] = "Section";
$lang['profile_room'] = "Chambre";
$lang['profile_highest_mark'] = "Plus Marque";
$lang['profile_point'] = "Point";
$lang['profile_grade'] = "Grade";
$lang['profile_total_marks'] = "Total Marques";
$lang['profile_average_marks'] = "Moyenne Marques";
$lang['profile_average_point'] = "Moyenne Point";
$lang['profile_average_grade'] = "Moyenne Grade";
$lang['profile_feetype'] = "Frais Type";
$lang['profile_fees_amount'] = "Les Frais D' Montant";
$lang['profile_date'] = "Date";
$lang['profile_discount'] = "Rabais";
$lang['profile_paid'] = "Payé";
$lang['profile_weaver'] = "Weaver";
$lang['profile_due'] = "En Raison";
$lang['profile_fine'] = "Fine";
$lang['profile_status'] = "Statut";
$lang['profile_notpaid'] = "Pas Payé";
$lang['profile_partially_paid'] = "Partiellement Payé";
$lang['profile_fully_paid'] = "Entièrement Payé";
$lang['profile_paymentmethod'] = "Paiement Méthode";
$lang['profile_type'] = "Type";
$lang['profile_to'] = "Pour";
$lang['profile_subject'] = "Sujet";
$lang['profile_message'] = "Message";
$lang['profile_data_not_found'] = "N'%27t   Données.";
$lang['profile_permissionmethod'] = "Méthode Pas Permis";
$lang['profile_permission'] = "Autorisation Pas Permis";
$lang['profile_total_holiday'] = "Total Vacances";
$lang['profile_total_weekenday'] = "Total Weekenday";
$lang['profile_total_present'] = "Total Présent";
$lang['profile_total_latewithexcuse'] = "Total Fin Avec Une Excuse";
$lang['profile_total_late'] = "Total Tard";
$lang['profile_total_absent'] = "Total Absent";
