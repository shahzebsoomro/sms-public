<?php

$lang['panel_title'] = "E-books";
$lang['slno'] = "#";
$lang['ebooks_name'] = "Nom";
$lang['ebooks_author'] = "Auteur";
$lang['ebooks_classes'] = "Classe";
$lang['ebooks_authority'] = "L'autorité";
$lang['ebooks_private'] = "Privé";
$lang['ebooks_cover_photo'] = "Couverture Photo";
$lang['ebooks_file'] = "Fichier";
$lang['action'] = "Action";
$lang['ebooks_select_class'] = "Sélectionnez Classe";
$lang['ebooks_select_department'] = "Sélectionnez Département";
$lang['ebooks_select_teacher'] = "Sélectionnez Enseignant";
$lang['view'] = "Vue";
$lang['edit'] = "Modifier";
$lang['delete'] = "Supprimer";
$lang['add_ebooks'] = "Ajouter Ebook";
$lang['add_title'] = "Ajouter Ebook";
$lang['update_ebooks'] = "Mise À Jour Ebook";
