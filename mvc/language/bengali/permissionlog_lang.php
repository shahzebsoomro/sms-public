<?php

$lang['panel_title'] = "Permissionlog";
$lang['add_title'] = "যোগ একটি Permissionlog";
$lang['slno'] = "#";
$lang['permissionlog_name'] = "নাম";
$lang['permissionlog_description'] = "বিবরণ:";
$lang['permissionlog_active'] = "সক্রিয়";
$lang['permissionlog_add'] = "যোগ";
$lang['action'] = "কর্ম";
$lang['view'] = "দেখুন";
$lang['edit'] = "সম্পাদনা";
$lang['delete'] = "মুছে দিন";
$lang['print'] = "প্রিন্ট";
$lang['pdf_preview'] = "Pdf সম্পূর্ণ বিবরণের পূর্বরূপ দেখুন";
$lang["mail"] = "পাঠান পিডিএফ     মেইল";
$lang['add_class'] = "যোগ Permissionlog";
$lang['update_class'] = "আপডেট Permissionlog";
$lang['to'] = "থেকে";
$lang['subject'] = "বিষয়";
$lang['message'] = "বার্তা";
$lang['send'] = "পাঠান";
$lang['mail_to'] = "ক্ষেত্র  প্রয়োজন.";
$lang['mail_valid'] = "ক্ষেত্র আবশ্যক ধারণ A বৈধ ইমেইল ঠিকানা.";
$lang['mail_subject'] = "Subject ক্ষেত্র  প্রয়োজন.";
$lang['mail_success'] = "ইমেল পাঠান সফলভাবে%2c";
$lang['mail_error'] = "ওহো%2c ইমেইল না পাঠান%2c";
