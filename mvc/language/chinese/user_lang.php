<?php

$lang['panel_title'] = "用户";
$lang['add_title'] = "添加 A 用户";
$lang['slno'] = "#";
$lang['user_photo'] = "照片";
$lang['user_name'] = "名称";
$lang['user_email'] = "电子邮件";
$lang['user_dob'] = "日 Of 生";
$lang['user_sex'] = "性别问题";
$lang['user_sex_male'] = "男性";
$lang['user_sex_female'] = "女性";
$lang['user_religion'] = "宗教";
$lang['user_phone'] = "电话";
$lang['user_address'] = "地址";
$lang['user_jod'] = "加入 日期";
$lang['user_usertype'] = "作用";
$lang['user_username'] = "用户名";
$lang['user_password'] = "密码";
$lang['user_status'] = "状态";
$lang['user_designation'] = "指定";
$lang['user_designation'] = "指定";
$lang['user_select_usertype'] = "选角色";
$lang['user_file_browse'] = "文件 浏览";
$lang['user_clear'] = "清楚的";
$lang['user_quick_add'] = "快速增加";
$lang['action'] = "动作";
$lang['view'] = "看";
$lang['edit'] = "编辑";
$lang['delete'] = "删除";
$lang['pdf_preview'] = "Pdf 预览";
$lang['idcard'] = "Id卡";
$lang['print'] = "印";
$lang['download'] = "下载";
$lang["mail"] = "送 Pdf To 邮件";
$lang['personal_information'] = "个人 信息";
$lang['add_user'] = "添加 用户";
$lang['update_user'] = "更新 用户";
$lang['to'] = "要";
$lang['subject'] = "受";
$lang['message'] = "消息";
$lang['send'] = "发送";
$lang['mail_to'] = "To 领域 Is 必需的。";
$lang['mail_valid'] = "To 领域 必须 包含 A 的有效电子邮件 地址。";
$lang['mail_subject'] = "受 领域 Is 必需的。";
$lang['mail_success'] = "电子邮件发送 成功%2c";
$lang['mail_error'] = "哎呀%2c 电子邮件 不 发%2c";
$lang['user_title'] = "标题";
$lang['user_date'] = "日期";
$lang['user_add_document'] = "增加的文件";
$lang['user_document_upload'] = "文件 上传";
$lang['user_file'] = "文件";
$lang['user_upload'] = "上传";
$lang['user_title_required'] = "题 领域 Is 必需的。";
$lang['user_file_required'] = "文件 领域 Is 必需的。";
$lang['user_profile'] = "配置文件";
$lang['user_attendance'] = "出勤";
$lang['user_salary'] = "薪水";
$lang['user_payment'] = "付款";
$lang['user_document'] = "文档";
$lang['user_1'] = "一个";
$lang['user_2'] = "两个";
$lang['user_3'] = "三";
$lang['user_4'] = "四个";
$lang['user_5'] = "五个";
$lang['user_6'] = "六个";
$lang['user_7'] = "七个";
$lang['user_8'] = "八个";
$lang['user_9'] = "九";
$lang['user_10'] = "十个";
$lang['user_11'] = "十一个";
$lang['user_12'] = "十二";
$lang['user_13'] = "十三";
$lang['user_14'] = "十四";
$lang['user_15'] = "十五";
$lang['user_16'] = "十六个";
$lang['user_17'] = "十七岁";
$lang['user_18'] = "十八";
$lang['user_19'] = "十九";
$lang['user_20'] = "二十";
$lang['user_21'] = "二十一";
$lang['user_22'] = "二十两";
$lang['user_23'] = "二十三";
$lang['user_24'] = "二十四";
$lang['user_25'] = "第二十五";
$lang['user_26'] = "第二十六";
$lang['user_27'] = "第二十七";
$lang['user_28'] = "第二十八";
$lang['user_29'] = "二十九";
$lang['user_30'] = "三十";
$lang['user_31'] = "第三十一";
$lang['user_salary_grades'] = "工资等级";
$lang['user_basic_salary'] = "基本 薪水";
$lang['user_overtime_rate'] = "加班 率 %28 每小时%29";
$lang['user_overtime_rate_not_hour'] = "加班 率";
$lang['user_allowances'] = "津贴";
$lang['user_deductions'] = "Dllowances";
$lang['user_total_salary_details'] = "总 工资 细节";
$lang['user_gross_salary'] = "总 薪水";
$lang['user_total_deduction'] = "总 扣";
$lang['user_net_salary'] = "净 薪水";
$lang['user_hourly_rate'] = "每小时 率";
$lang['user_net_hourly_rate'] = "净 每小时 率";
$lang['user_net_salary_hourly'] = "净 工资 %28hourly%29";
$lang['user_month'] = "一个月";
$lang['user_payment_amount'] = "支付 量";
$lang['user_total'] = "总";
$lang['user_to'] = "要";
$lang['user_subject'] = "受";
$lang['user_message'] = "消息";
$lang['user_userID'] = "用户 Id";
$lang['user_data_not_found'] = "不%27t  任何 数据。";
$lang['user_permissionmethod'] = "方法 不 允许";
$lang['user_permission'] = "权限 不 允许";
$lang['user_type'] = "类型";
$lang['user_total_holiday'] = "总 度假";
$lang['user_total_weekenday'] = "总 Weekenday";
$lang['user_total_present'] = "总 本";
$lang['user_total_latewithexcuse'] = "总 晚 With 借口";
$lang['user_total_late'] = "总 晚";
$lang['user_total_absent'] = "总 不存在";
