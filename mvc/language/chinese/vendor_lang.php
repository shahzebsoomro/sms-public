<?php

$lang['panel_title'] = "供应商";
$lang['add_title'] = "添加 A 供应商";
$lang['slno'] = "#";
$lang['vendor_name'] = "名称";
$lang['vendor_phone'] = "电话";
$lang['vendor_email'] = "电子邮件";
$lang['vendor_contact_name'] = "联系的名字";
$lang['vendor_add'] = "添加";
$lang['action'] = "动作";
$lang['view'] = "看";
$lang['edit'] = "编辑";
$lang['delete'] = "删除";
$lang['print'] = "印";
$lang['pdf_preview'] = "Pdf 预览";
$lang["mail"] = "送 Pdf To 邮件";
$lang['add_vendor'] = "增加供应商";
$lang['update_vendor'] = "更新 供应商";
$lang['to'] = "";
$lang['subject'] = "受";
$lang['message'] = "消息";
$lang['send'] = "发送";
$lang['mail_to'] = "To 领域 Is 必需的。";
$lang['mail_valid'] = "To 领域 必须 包含 A 的有效电子邮件 地址。";
$lang['mail_subject'] = "受 领域 Is 必需的。";
$lang['mail_success'] = "电子邮件发送 成功%2c";
$lang['mail_error'] = "哎呀%2c 电子邮件 不 发%2c";
