<?php

$lang['panel_title'] = "老师";
$lang['add_title'] = "添加 A 老师";
$lang['slno'] = "#";
$lang['teacher_photo'] = "照片";
$lang['teacher_name'] = "名称";
$lang['teacher_designation'] = "指定";
$lang['teacher_email'] = "电子邮件";
$lang['teacher_dob'] = "日 Of 生";
$lang['teacher_sex'] = "性别问题";
$lang['teacher_sex_male'] = "男性";
$lang['teacher_sex_female'] = "女性";
$lang['teacher_religion'] = "宗教";
$lang['teacher_phone'] = "电话";
$lang['teacher_address'] = "地址";
$lang['teacher_jod'] = "加入 日期";
$lang['teacher_username'] = "用户名";
$lang['teacher_password'] = "密码";
$lang['teacher_status'] = "状态";
$lang['teacher_file_browse'] = "文件 浏览";
$lang['teacher_clear'] = "清楚的";
$lang['teacher_profile'] = "配置文件";
$lang['teacher_routine'] = "例行程序";
$lang['teacher_attendance'] = "出勤";
$lang['teacher_mark'] = "标记";
$lang['teacher_invoice'] = "发票";
$lang['teacher_salary'] = "薪水";
$lang['teacher_payment'] = "付款";
$lang['teacher_document'] = "文档";
$lang['action'] = "动作";
$lang['view'] = "看";
$lang['edit'] = "编辑";
$lang['delete'] = "删除";
$lang['print'] = "印";
$lang['pdf_preview'] = "Pdf 预览";
$lang['idcard'] = "Id卡";
$lang['mail'] = "送 Pdf To 邮件";
$lang['download'] = "下载";
$lang['personal_information'] = "个人 信息";
$lang['add_teacher'] = "增加的老师";
$lang['update_teacher'] = "更新 老师";
$lang['to'] = "要";
$lang['subject'] = "受";
$lang['message'] = "消息";
$lang['send'] = "发送";
$lang['mail_to'] = "To 领域 Is 必需的。";
$lang['mail_valid'] = "To 领域 必须 包含 A 的有效电子邮件 地址。";
$lang['mail_subject'] = "受 领域 Is 必需的。";
$lang['mail_success'] = "电子邮件发送 成功%2c";
$lang['mail_error'] = "哎呀%2c 电子邮件 不 发%2c";
$lang['sunday'] = "周日";
$lang['monday'] = "星期一";
$lang['tuesday'] = "星期二";
$lang['wednesday'] = "星期三";
$lang['thursday'] = "星期四";
$lang['friday'] = "星期五";
$lang['saturday'] = "星期六";
$lang["teacher_day"] = "一天";
$lang["teacher_period"] = "周期";
$lang["teacher_subject"] = "受";
$lang["teacher_room"] = "房间";
$lang["teacher_class"] = "类";
$lang["teacher_section"] = "部分";
$lang['teacher_1'] = "一个";
$lang['teacher_2'] = "两个";
$lang['teacher_3'] = "三";
$lang['teacher_4'] = "四个";
$lang['teacher_5'] = "五个";
$lang['teacher_6'] = "六个";
$lang['teacher_7'] = "七个";
$lang['teacher_8'] = "八个";
$lang['teacher_9'] = "九";
$lang['teacher_10'] = "十个";
$lang['teacher_11'] = "十一个";
$lang['teacher_12'] = "十二";
$lang['teacher_13'] = "十三";
$lang['teacher_14'] = "十四";
$lang['teacher_15'] = "十五";
$lang['teacher_16'] = "十六个";
$lang['teacher_17'] = "十七岁";
$lang['teacher_18'] = "十八";
$lang['teacher_19'] = "十九";
$lang['teacher_20'] = "二十";
$lang['teacher_21'] = "二十一";
$lang['teacher_22'] = "二十两";
$lang['teacher_23'] = "二十三";
$lang['teacher_24'] = "二十四";
$lang['teacher_25'] = "第二十五";
$lang['teacher_26'] = "第二十六";
$lang['teacher_27'] = "第二十七";
$lang['teacher_28'] = "第二十八";
$lang['teacher_29'] = "二十九";
$lang['teacher_30'] = "三十";
$lang['teacher_31'] = "第三十一";
$lang['teacher_salary_grades'] = "工资等级";
$lang['teacher_basic_salary'] = "基本 薪水";
$lang['teacher_overtime_rate'] = "加班 率 %28 每小时%29";
$lang['teacher_overtime_rate_not_hour'] = "加班 率";
$lang['teacher_allowances'] = "津贴";
$lang['teacher_deductions'] = "Dllowances";
$lang['teacher_total_salary_details'] = "总 工资 细节";
$lang['teacher_gross_salary'] = "总 薪水";
$lang['teacher_total_deduction'] = "总 扣";
$lang['teacher_net_salary'] = "净 薪水";
$lang['teacher_hourly_rate'] = "每小时 率";
$lang['teacher_net_hourly_rate'] = "净 每小时 率";
$lang['teacher_net_salary_hourly'] = "净 工资 %28hourly%29";
$lang['teacher_month'] = "一个月";
$lang['teacher_payment_amount'] = "支付 量";
$lang['teacher_total'] = "总";
$lang['teacher_add_document'] = "增加的文件";
$lang['teacher_document_upload'] = "文件 上传";
$lang['teacher_title'] = "标题";
$lang['teacher_file'] = "文件";
$lang['teacher_upload'] = "上传";
$lang['teacher_title_required'] = "题 领域 Is 必需的。";
$lang['teacher_file_required'] = "文件 领域 Is 必需的。";
$lang['teacher_date'] = "日期";
$lang['teacher_to'] = "要";
$lang['teacher_message'] = "消息";
$lang['teacher_teacherID'] = "老师 Id";
$lang['teacher_data_not_found'] = "不%27t  任何 数据。";
$lang['teacher_permissionmethod'] = "方法 不 允许";
$lang['teacher_permission'] = "权限 不 允许";
$lang['teacher_total_holiday'] = "总 度假";
$lang['teacher_total_weekenday'] = "总 Weekenday";
$lang['teacher_total_present'] = "总 本";
$lang['teacher_total_latewithexcuse'] = "晚 总 借口";
$lang['teacher_total_late'] = "总 晚";
$lang['teacher_total_absent'] = "总 不存在";
$lang['teacher_total_leave'] = "总 离开";
