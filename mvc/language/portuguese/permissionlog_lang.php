<?php

$lang['panel_title'] = "Permissionlog";
$lang['add_title'] = "Adicionar Um Permissionlog";
$lang['slno'] = "#";
$lang['permissionlog_name'] = "Nome";
$lang['permissionlog_description'] = "Descrição";
$lang['permissionlog_active'] = "Active";
$lang['permissionlog_add'] = "Adicionar";
$lang['action'] = "Ação";
$lang['view'] = "Visualizar";
$lang['edit'] = "Editar";
$lang['delete'] = "Apagar";
$lang['print'] = "Imprimir";
$lang['pdf_preview'] = "Pdf Pré-visualização";
$lang["mail"] = "Enviar Pdf Para Correio";
$lang['add_class'] = "Adicione Permissionlog";
$lang['update_class'] = "Atualização Permissionlog";
$lang['to'] = "Para";
$lang['subject'] = "Assunto";
$lang['message'] = "Mensagem";
$lang['send'] = "Enviar";
$lang['mail_to'] = "De Campo é Necessário.";
$lang['mail_valid'] = "De Campo Deve Conter Um Válidos E-mail Endereço.";
$lang['mail_subject'] = "O Assunto Campo é Necessário.";
$lang['mail_success'] = "E-mail Enviar Com êxito%2c";
$lang['mail_error'] = "Oops%2c E-mail Não Envie%2c";
