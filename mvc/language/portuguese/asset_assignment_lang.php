<?php

$lang['panel_title'] = "Ativo De Atribuição De";
$lang['add_title'] = "Adicionar Um Ativo De Atribuição De";
$lang['slno'] = "#";
$lang['asset_assignment_assetID'] = "Activo";
$lang['asset_assignment_select_usertype'] = "Escolha Papel";
$lang['select_asset'] = "Escolha Ativo";
$lang['status'] = "Estado";
$lang['asset_status_checked_in'] = "Em De Armazenamento";
$lang['asset_status_checked_out'] = "Verificada Out";
$lang['asset_assignment_select_class'] = "Selecione Classe";
$lang['asset_assignment_classesID'] = "Classe";
$lang['asset_assignment_select_status'] = "Selecione Estado";
$lang['asset_assignment_select_location'] = "Escolha Localização";
$lang['asset_assignment_note'] = "Nota";
$lang['asset_locationID'] = "Local";
$lang['check_in_date'] = "Verifique Em Data";
$lang['check_out_date'] = "Verifique Fora Data";
$lang['due_date'] = "Devido Data";
$lang['assigned_quantity'] = "Atribuir Quantidade";
$lang['asset_assignment_check_out_to'] = "Verifique Fora Para";
$lang['asset_assignment_usertypeID'] = "Papel";
$lang['asset_assignment_add'] = "Adicionar";
$lang['action'] = "Ação";
$lang['view'] = "Visualizar";
$lang['edit'] = "Editar";
$lang['delete'] = "Apagar";
$lang['print'] = "Imprimir";
$lang['pdf_preview'] = "Pdf Pré-visualização";
$lang["mail"] = "Enviar Pdf Para Correio";
$lang['add_asset_assignment'] = "Adicione Ativo De Atribuição De";
$lang['update_asset_assingment'] = "Atualização Ativo De Atribuição De";
$lang['to'] = "Para";
$lang['subject'] = "Assunto";
$lang['message'] = "Mensagem";
$lang['send'] = "Enviar";
$lang['mail_to'] = "De Campo é Necessário.";
$lang['mail_valid'] = "De Campo Deve Conter Um Válidos E-mail Endereço.";
$lang['mail_subject'] = "O Assunto Campo é Necessário.";
$lang['mail_success'] = "E-mail Enviar Com êxito%2c";
$lang['mail_error'] = "Oops%2c E-mail Não Envie%2c";
$lang['asset_assignment_select_user'] = "Escolha Usuário";
