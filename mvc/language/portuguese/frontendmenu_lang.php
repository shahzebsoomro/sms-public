<?php

$lang['panel_title'] = "Frontend Menu";
$lang['add_title'] = "Adicionar Um Atividades";
$lang['slno'] = "#";
$lang['activities_title'] = "Título";
$lang['activities_description'] = "Descrição";
$lang['activities_students'] = "Alunos";
$lang['activities_shared_publicly'] = "Compartilhado Publicamente";
$lang['file_browse'] = "Procurar Arquivo";
$lang['attachment'] = "Anexo";
$lang['clear'] = "Claro";
$lang['activities_time_frame'] = "Tempo Quadro";
$lang['activities_time_from'] = "Tempo De";
$lang['activities_time_to'] = "Tempo Para";
$lang['activities_time_at'] = "Tempo A";
$lang['students'] = "Aluno Lista";
$lang['activities_create_date'] = "Criar Data";
$lang['action'] = "Ação";
$lang['edit'] = "Editar";
$lang['delete'] = "Apagar";
$lang['add_activities'] = "Adicione Atividades";
$lang['update_activities'] = "Atualização Atividades";
$lang['frontendmenu_edit_menus'] = "Edit Menus";
$lang['frontendmenu_select_a_menu_to_edit'] = "Escolha Uma Menu Para Editar";
$lang['frontendmenu_manage_locations'] = "Gerenciar Loacations";
$lang['frontendmenu_top_menu'] = "Topo Menu";
$lang['frontendmenu_social_links_menu'] = "Social Links Menu";
$lang['frontendmenu_select'] = "Seleccione";
$lang['frontendmenu_pages'] = "Páginas";
$lang['frontendmenu_posts'] = "Postos De";
$lang['frontendmenu_custom_links'] = "Personalizado Links";
$lang['frontendmenu_custom_link'] = "Personalizado Link";
$lang['frontendmenu_view_all'] = "Ler Todos Os";
$lang['frontendmenu_select_all'] = "Escolha Todos Os";
$lang['frontendmenu_url'] = "Url";
$lang['frontendmenu_link_text'] = "Link Texto";
$lang['frontendmenu_pending'] = "Pendente";
$lang['frontendmenu_page'] = "Página";
$lang['frontendmenu_post'] = "Postar";
$lang['frontendmenu_navigation_label'] = "Navegação Etiqueta";
$lang['frontendmenu_move'] = "Mover";
$lang['frontendmenu_up_one'] = "Até";
$lang['frontendmenu_down_one'] = "Baixo Uma";
$lang['frontendmenu_orginal'] = "Original";
$lang['frontendmenu_no_label'] = "Não Etiqueta";
$lang['frontendmenu_remove'] = "Remover";
$lang['frontendmenu_cancel'] = "Cancelar";
$lang['frontendmenu_add_to_menu'] = "Adicionar Ao Menu";
$lang['frontendmenu_save_menu'] = "Guardar Menu";
$lang['frontendmenu_save_changes'] = "Guardar Alterações";
$lang['frontendmenu_menu_name'] = "Menu Nome";
$lang['frontendmenu_menu_structure'] = "Menu Estrutura";
$lang['frontendmenu_menu_structure_desc'] = "Drag Each Item Into The Order You Prefer. Click The Arrow On The Right Of The Item To Reveal Additional Configuration Options.";
$lang['frontendmenu_display_location'] = "Localização";
$lang['frontendmenu_menu_settings'] = "Menu Configuração De";
$lang['frontendmenu_delete_menu'] = "Excluir Menu";
$lang['frontendmenu_select_a_menu'] = "Escolha Uma Menu";
$lang['frontendmenu_error_label'] = "A Rótulo Campo Pode Ser Superior A 253 Personagens Em De Comprimento.";
$lang['frontendmenu_error_menu'] = "Por Favor, Criar Um Menu.";
$lang['frontendmenu_or'] = "Ou";
$lang['frontendmenu_create_a_new_menu'] = "Criar Um Novo Menu.";
$lang['frontendmenu_create_menu'] = "Criar Menu";
$lang['frontendmenu_create_menu_desc'] = "Dar Seu Menu Um Nome%2c Depois Clique Em Criar Menu.";
$lang['frontendmenu_edit_your_menu_below'] = "Edit Seu Menu Abaixo";
$lang['frontendmenu_error_menu_required'] = "A Menu Nome Campo é Necessário.";
$lang['frontendmenu_error_menu_length'] = "A Menu Nome Campo Pode Ser Superior A 128 Personagens Em De Comprimento.";
