<?php

$lang['panel_title'] = "O Visitante Informações";
$lang['add_title'] = "Criar Passagem";
$lang['slno'] = "#";
$lang['action'] = "Ação";
$lang['view'] = "Visualizar";
$lang['edit'] = "Editar";
$lang['delete'] = "Apagar";
$lang['visitor_usrtype'] = "Para Atender Usuário Tipo";
$lang['visitor_select_usertype'] = "Escolha Papel";
$lang['visitor_select_user'] = "Escolha Usuário";
$lang['visitor_info'] = "O Visitante Informações";
$lang['visitorID'] = "O Visitante Id";
$lang['name'] = "Nome";
$lang['email_id'] = "E-mail";
$lang['phone'] = "Telefone";
$lang['company_name'] = "Empresa";
$lang['coming_from'] = "Vem De";
$lang['to_meet'] = "Para Atender";
$lang['check_in'] = "Verifique";
$lang['check_out'] = "Verifique Out";
$lang['status'] = "Estado";
$lang['visitor_f_name'] = "Primeiro Nome";
$lang['print_pass'] = "Imprimir";
$lang['representing'] = "Representando";
$lang['cancel'] = "Cancelar";
$lang['photo'] = "O Visitante Foto";
$lang['visitor_list'] = "O Visitante Lista";
$lang['visitor_add'] = "O Visitante Adicionar";
$lang['upload_error'] = "Arquivo Não Carregado%2c";
$lang['upload_error_data'] = "Dados Não %2c";
$lang['upload_success'] = "Dados Sucesso %2c";
$lang['checkout_success'] = "Checkout Bem-sucedida%2c";
$lang['checkout_error'] = "Checkout Incompleta%2c";
$lang['invalid_id'] = "Inválido Visitantes Id%2c";
$lang['admin_select_label'] = "Admin Lista";
$lang['student_select_label'] = "Estudantes Lista";
$lang['parent_select_label'] = "Pais Lista";
$lang['teacher_select_label'] = "Professores Lista";
$lang['librarian_select_label'] = "Os Bibliotecários Lista";
$lang['accountant_select_label'] = "Contabilistas Lista";
$lang['visitor_checkout'] = "O Visitante Verificar Out";
$lang['logout'] = "Logout";
