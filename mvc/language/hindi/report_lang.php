<?php

$lang['panel_title'] = "रिपोर्ट";
$lang['report_print'] = "प्रिंट";
$lang['report_report_for'] = "रिपोर्ट के लिए";
$lang['report_blood_group'] = "रक्त समूह";
$lang['report_country'] = "देश";
$lang['report_school_or_class'] = "स्कूल%2fclass";
$lang['report_school'] = "स्कूल";
$lang['report_student'] = "छात्र";
$lang['report_class'] = "वर्ग";
$lang['report_attendance'] = "उपस्थिति";
$lang['report_attendancetype'] = "उपस्थिति प्रकार";
$lang['report_section'] = "अनुभाग";
$lang['report_submit'] = "प्राप्त रिपोर्ट";
$lang['report_select_class'] = "का चयन करें वर्ग";
$lang['report_select_section'] = "का चयन करें धारा";
$lang['report_select_subject'] = "का चयन करें विषय";
$lang['report_please_select'] = "कृपया का चयन करें";
$lang['report_select_attendancetype'] = "का चयन करें उपस्थिति प्रकार";
$lang['report_select_all_section'] = "सभी धारा";
$lang['report_class_info'] = "वर्ग Informations";
$lang['report_class_number_of_students'] = "नंबर के छात्रों";
$lang['report_class_total_subject_assigned'] = "कुल विषय सौंपा";
$lang['report_class_teacher'] = "वर्ग शिक्षक";
$lang['report_transport'] = "परिवहन";
$lang['report_route'] = "मार्ग";
$lang['report_hostel'] = "छात्रावास";
$lang['report_for'] = "के लिए";
$lang['report_phone'] = "फोन";
$lang['report_date'] = "तारीख";
$lang['report_photo'] = "फोटो";
$lang['report_name'] = "नाम";
$lang['report_gender'] = "लिंग";
$lang['report_male'] = "पुरुष";
$lang['report_female'] = "महिला";
$lang['report_roll'] = "रोल";
$lang['report_email'] = "ईमेल";
$lang['report_address'] = "पता";
$lang['report_subject'] = "विषय";
$lang['report_teacher'] = "शिक्षक";
$lang['report_feetype'] = "शुल्क प्रकार";
$lang['report_student'] = "छात्र";
$lang['report_student_roll'] = "रोल";
$lang['report_present'] = "वर्तमान";
$lang['report_absent'] = "अनुपस्थित";
$lang['report_collection'] = "संग्रह";
$lang['report_due'] = "कारण";
$lang['report_due_amount'] = "कारण राशि";
$lang['report_total_amount'] = "कुल राशि";
$lang['report_paid_amount'] = "भुगतान राशि";
$lang['report_subject_and_teachers'] = "विषयों और शिक्षकों";
$lang['report_feetypes_collection'] = "शुल्क प्रकार के संग्रह";
$lang['report_student_account_info'] = "छात्र खाते जानकारी";
$lang['report_feetype_details'] = "शुल्क विवरण";
$lang['report_chart_not_found'] = "चार्ट नहीं उपलब्ध";
$lang['report_student_not_found'] = "डॉन%27t है भी छात्रों.";
$lang['report_certificate'] = "प्रमाण पत्र";
$lang['report_select_academic_year'] = "का चयन करें अकादमिक वर्ष";
$lang['report_select_template'] = "का चयन करें टेम्पलेट";
$lang['report_academic_year'] = "शैक्षणिक वर्ष";
$lang['report_template'] = "टेम्पलेट";
$lang['report_generate_certificate'] = "उत्पन्न प्रमाण पत्र";
$lang['report_action'] = "कार्रवाई";
