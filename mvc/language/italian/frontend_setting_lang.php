<?php

$lang['panel_title'] = "Frontend Impostazioni";
$lang['frontend_setting_facebook'] = "Facebook";
$lang['frontend_setting_twitter'] = "Twitter";
$lang['frontend_setting_linkedin'] = "Linkedin";
$lang['frontend_setting_youtube'] = "Youtube";
$lang['frontend_setting_google'] = "Google %2b";
$lang['frontend_setting_frontend_configaration'] = "Frontend Configaration";
$lang['frontend_setting_social'] = "Sociale";
$lang['frontend_setting_description'] = "Descrizione";
$lang['frontend_setting_teacher_email'] = "Insegnante Email";
$lang['frontend_setting_teacher_phone'] = "Insegnante Telefono";
$lang['frontend_setting_enable'] = "Attivare";
$lang['frontend_setting_disable'] = "Disattivare";
$lang['update_frontend_setting'] = "Aggiornamento Frontend Impostazione";
