<?php

$lang['panel_title'] = "E-book";
$lang['slno'] = "#";
$lang['ebooks_name'] = "Nome";
$lang['ebooks_author'] = "Autore";
$lang['ebooks_classes'] = "Classe";
$lang['ebooks_authority'] = "Autorità";
$lang['ebooks_private'] = "Privato";
$lang['ebooks_cover_photo'] = "Copertina Foto";
$lang['ebooks_file'] = "File";
$lang['action'] = "Azione";
$lang['ebooks_select_class'] = "Selezionare Classe";
$lang['ebooks_select_department'] = "Selezionare Dipartimento";
$lang['ebooks_select_teacher'] = "Selezionare Insegnante";
$lang['view'] = "Vista";
$lang['edit'] = "Modifica";
$lang['delete'] = "Eliminare";
$lang['add_ebooks'] = "Aggiungere Ebook";
$lang['add_title'] = "Aggiungere Ebook";
$lang['update_ebooks'] = "Aggiornamento Ebook";
