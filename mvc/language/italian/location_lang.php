<?php

$lang['panel_title'] = "Posizione";
$lang['add_title'] = "Aggiungere Un Posizione";
$lang['slno'] = "#";
$lang['location'] = "Posizione";
$lang['location_description'] = "Descrizione";
$lang['location_add'] = "Aggiungere";
$lang['action'] = "Azione";
$lang['view'] = "Vista";
$lang['edit'] = "Modifica";
$lang['delete'] = "Eliminare";
$lang['print'] = "Stampa";
$lang['pdf_preview'] = "Pdf Anteprima";
$lang["mail"] = "Inviare Pdf A Mail";
$lang['add_location'] = "Aggiungere Posizione";
$lang['update_location'] = "Aggiornamento Posizione";
$lang['to'] = "Per";
$lang['subject'] = "Soggetto";
$lang['message'] = "Messaggio";
$lang['send'] = "Inviare";
$lang['mail_to'] = "Il Al Campo è Necessario.";
$lang['mail_valid'] = "Il Al Campo Deve Contiene Un Valido Email Indirizzo.";
$lang['mail_subject'] = "Il Soggetto Campo è Necessario.";
$lang['mail_success'] = "E-mail Inviare Con Successo%2c";
$lang['mail_error'] = "Oops%2c E-mail Non Inviare%2c";
