<?php

$lang['panel_title'] = "Profesor";
$lang['add_title'] = "Adaugă O Profesor";
$lang['slno'] = "#";
$lang['teacher_photo'] = "Foto";
$lang['teacher_name'] = "Numele";
$lang['teacher_designation'] = "Denumire";
$lang['teacher_email'] = "E-mail";
$lang['teacher_dob'] = "Data De Nastere";
$lang['teacher_sex'] = "Gen";
$lang['teacher_sex_male'] = "De Sex Masculin";
$lang['teacher_sex_female'] = "De Sex Feminin";
$lang['teacher_religion'] = "Religia";
$lang['teacher_phone'] = "Telefon";
$lang['teacher_address'] = "Adresa";
$lang['teacher_jod'] = "Aderarea Data";
$lang['teacher_username'] = "Numele De Utilizator";
$lang['teacher_password'] = "Parola";
$lang['teacher_status'] = "Starea";
$lang['teacher_file_browse'] = "Fișier A Naviga";
$lang['teacher_clear'] = "Clar";
$lang['teacher_profile'] = "Profil";
$lang['teacher_routine'] = "Rutina";
$lang['teacher_attendance'] = "Participarea";
$lang['teacher_mark'] = "Mark";
$lang['teacher_invoice'] = "Factura";
$lang['teacher_salary'] = "Salariu";
$lang['teacher_payment'] = "Plata";
$lang['teacher_document'] = "Document";
$lang['action'] = "Acțiune";
$lang['view'] = "Vedere";
$lang['edit'] = "Edit";
$lang['delete'] = "Șterge";
$lang['print'] = "Print";
$lang['pdf_preview'] = "Pdf Previzualizare";
$lang['idcard'] = "Id Card";
$lang['mail'] = "Trimite Pdf A E-mail";
$lang['download'] = "Download";
$lang['personal_information'] = "Personal Informații";
$lang['add_teacher'] = "Adaugă Profesor";
$lang['update_teacher'] = "Actualizare Profesor";
$lang['to'] = "Pentru A";
$lang['subject'] = "Subiect";
$lang['message'] = "Mesaj";
$lang['send'] = "Trimite";
$lang['mail_to'] = "De Teren Este Necesar.";
$lang['mail_valid'] = "De Teren Trebuie Conțin O Valabil E-mail Adresa.";
$lang['mail_subject'] = "Subiect Teren Este Necesar.";
$lang['mail_success'] = "E-mail Trimite Cu Succes%2c";
$lang['mail_error'] = "Oops%2c E-mail Nu Trimite%2c";
$lang['sunday'] = "DuminicĂ";
$lang['monday'] = "Luni";
$lang['tuesday'] = "MarȚi";
$lang['wednesday'] = "Miercuri";
$lang['thursday'] = "Joi";
$lang['friday'] = "Vineri";
$lang['saturday'] = "SÂmbĂtĂ";
$lang["teacher_day"] = "Zi";
$lang["teacher_period"] = "Perioada";
$lang["teacher_subject"] = "Subiect";
$lang["teacher_room"] = "Cameră";
$lang["teacher_class"] = "Clasa";
$lang["teacher_section"] = "Secțiunea";
$lang['teacher_1'] = "Unul";
$lang['teacher_2'] = "Două";
$lang['teacher_3'] = "Trei";
$lang['teacher_4'] = "Patru";
$lang['teacher_5'] = "Cinci";
$lang['teacher_6'] = "Șase";
$lang['teacher_7'] = "Șapte";
$lang['teacher_8'] = "Opt";
$lang['teacher_9'] = "Nouă";
$lang['teacher_10'] = "Zece";
$lang['teacher_11'] = "Unsprezece";
$lang['teacher_12'] = "Doisprezece";
$lang['teacher_13'] = "Treisprezece";
$lang['teacher_14'] = "Paisprezece";
$lang['teacher_15'] = "Cincisprezece";
$lang['teacher_16'] = "Șaisprezece";
$lang['teacher_17'] = "Șaptesprezece";
$lang['teacher_18'] = "Optsprezece";
$lang['teacher_19'] = "Nouăsprezece";
$lang['teacher_20'] = "Douăzeci De";
$lang['teacher_21'] = "Douăzeci și Una De";
$lang['teacher_22'] = "Douăzeci și Doi De";
$lang['teacher_23'] = "";
$lang['teacher_24'] = "Douăzeci și Patru";
$lang['teacher_25'] = "Douăzeci și Cinci De";
$lang['teacher_26'] = "Douăzeci și șase";
$lang['teacher_27'] = "Douăzeci și șapte De";
$lang['teacher_28'] = "Douăzeci și Opt";
$lang['teacher_29'] = "Douăzeci și Nouă";
$lang['teacher_30'] = "Treizeci De";
$lang['teacher_31'] = "Treizeci și Unu";
$lang['teacher_salary_grades'] = "Salariu Note";
$lang['teacher_basic_salary'] = "De Bază Salariu";
$lang['teacher_overtime_rate'] = "Ore Suplimentare Rata %28 Pe Oră%29";
$lang['teacher_overtime_rate_not_hour'] = "Ore Suplimentare Rata";
$lang['teacher_allowances'] = "Indemnizațiile";
$lang['teacher_deductions'] = "Dllowances";
$lang['teacher_total_salary_details'] = "Total Salariu Detalii";
$lang['teacher_gross_salary'] = "Brut Salariu";
$lang['teacher_total_deduction'] = "Total Deducere";
$lang['teacher_net_salary'] = "Net Salariu";
$lang['teacher_hourly_rate'] = "Detalii Rata";
$lang['teacher_net_hourly_rate'] = "Net Orare Rata";
$lang['teacher_net_salary_hourly'] = "Net Salariu %28hourly%29";
$lang['teacher_month'] = "Luna";
$lang['teacher_payment_amount'] = "De Plată Cantitate";
$lang['teacher_total'] = "Total";
$lang['teacher_add_document'] = "Adaugă Document";
$lang['teacher_document_upload'] = "Document Incarca";
$lang['teacher_title'] = "Titlu";
$lang['teacher_file'] = "Fișier";
$lang['teacher_upload'] = "Incarca";
$lang['teacher_title_required'] = "Titlul Teren Este Necesar.";
$lang['teacher_file_required'] = "Dosar Teren Este Necesar.";
$lang['teacher_date'] = "Data";
$lang['teacher_to'] = "Pentru A";
$lang['teacher_message'] = "Mesaj";
$lang['teacher_teacherID'] = "Profesor Id";
$lang['teacher_data_not_found'] = "Nu%27t Au Orice Date.";
$lang['teacher_permissionmethod'] = "Metoda Nu Permis";
$lang['teacher_permission'] = "Permisiunea Nu Permis";
$lang['teacher_total_holiday'] = "Total De Vacanță";
$lang['teacher_total_weekenday'] = "Total Weekenday";
$lang['teacher_total_present'] = "Total Cadou";
$lang['teacher_total_latewithexcuse'] = "Târziu Total Scuză";
$lang['teacher_total_late'] = "Total Târziu";
$lang['teacher_total_absent'] = "Total Absent";
$lang['teacher_total_leave'] = "Total Lasă";
