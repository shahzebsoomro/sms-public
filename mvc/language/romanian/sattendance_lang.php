<?php

$lang['panel_title'] = "Student Prezență";
$lang['panel_title2'] = "Participarea";
$lang['add_title'] = "Adaugă Student Prezență";
$lang['slno'] = "#";
$lang['attendance_classes'] = "Clasa";
$lang['attendance_section'] = "Secțiunea";
$lang['attendance_subject'] = "Subiect";
$lang['attendance_student'] = "Student";
$lang['attendance_studentid'] = "Student Id";
$lang['attendance_date'] = "Data";
$lang['attendance_photo'] = "Foto";
$lang['attendance_name'] = "Numele";
$lang['attendance_section'] = "Secțiunea";
$lang['attendance_roll'] = "Rola";
$lang['attendance_phone'] = "Telefon";
$lang['attendance_dob'] = "Data De Nastere";
$lang['attendance_sex'] = "Gen";
$lang['attendance_religion'] = "Religia";
$lang['attendance_email'] = "E-mail";
$lang['attendance_address'] = "Adresa";
$lang['attendance_username'] = "Numele De Utilizator";
$lang['attendance_all_students'] = "Toate Elevi";
$lang['attendance_details'] = "Prezență Detalii";
$lang['attendance_day'] = "Zi";
$lang['attendance_registerNO'] = "Înregistrați Nu";
$lang['attendance_bloodgroup'] = "Sânge Grup";
$lang['attendance_state'] = "De Stat";
$lang['attendance_country'] = "Tara";
$lang['attendance_studentgroup'] = "Grup";
$lang['attendance_optionalsubject'] = "Optional Subiect";
$lang['attendance_extracurricularactivities'] = "Extra Școlare Activități";
$lang['attendance_remarks'] = "Observații";
$lang['attendance_attendance'] = "Participarea";
$lang['attendance_presentandabsent'] = "Prezent%2fabsent";
$lang['attendance_late'] = "Târziu";
$lang['attendance_select_classes'] = "Selectați Clasa A";
$lang['attendance_select_section'] = "Selectați Secțiunea";
$lang['attendance_select_subject'] = "Selectați Subiect";
$lang['attendance_select_student'] = "Selectați Pentru Studenți";
$lang['personal_information'] = "Personal Informații";
$lang['attendance_information'] = "Prezență Informații";
$lang['action'] = "Acțiune";
$lang['view'] = "Vedere";
$lang['pdf_preview'] = "Pdf Previzualizare";
$lang['print'] = "Print";
$lang["mail"] = "Trimite Pdf A E-mail";
$lang['add_attendance'] = "Participarea";
$lang['add_all_attendance'] = "Adaugă Toate Prezență";
$lang['add_all_attendance_late'] = "Adaugă Toate Târziu Prezență";
$lang['attendance_jan'] = "Ianuarie";
$lang['attendance_feb'] = "Februarie";
$lang['attendance_mar'] = "Martie";
$lang['attendance_apr'] = "Aprilie";
$lang['attendance_may'] = "Poate";
$lang['attendance_june'] = "Iunie";
$lang['attendance_jul'] = "Iulie";
$lang['attendance_aug'] = "August";
$lang['attendance_sep'] = "";
$lang['attendance_oct'] = "Octombrie";
$lang['attendance_nov'] = "Noiembrie";
$lang['attendance_dec'] = "Decembrie";
$lang['attendance_1'] = "Unul";
$lang['attendance_2'] = "Două";
$lang['attendance_3'] = "Trei";
$lang['attendance_4'] = "Patru";
$lang['attendance_5'] = "Cinci";
$lang['attendance_6'] = "Șase";
$lang['attendance_7'] = "Șapte";
$lang['attendance_8'] = "Opt";
$lang['attendance_9'] = "Nouă";
$lang['attendance_10'] = "Zece";
$lang['attendance_11'] = "Unsprezece";
$lang['attendance_12'] = "Doisprezece";
$lang['attendance_13'] = "Treisprezece";
$lang['attendance_14'] = "Paisprezece";
$lang['attendance_15'] = "Cincisprezece";
$lang['attendance_16'] = "Șaisprezece";
$lang['attendance_17'] = "Șaptesprezece";
$lang['attendance_18'] = "Optsprezece";
$lang['attendance_19'] = "Nouăsprezece";
$lang['attendance_20'] = "Douăzeci De";
$lang['attendance_21'] = "Douăzeci și Una De";
$lang['attendance_22'] = "Douăzeci și Doi De";
$lang['attendance_23'] = "Douăzeci și Trei";
$lang['attendance_24'] = "Douăzeci și Patru";
$lang['attendance_25'] = "Douăzeci și Cinci De";
$lang['attendance_26'] = "Douăzeci și șase";
$lang['attendance_27'] = "Douăzeci și șapte De";
$lang['attendance_28'] = "Douăzeci și Opt";
$lang['attendance_29'] = "Douăzeci și Nouă";
$lang['attendance_30'] = "Treizeci De";
$lang['attendance_31'] = "Treizeci și Unu";
$lang['to'] = "Pentru A";
$lang['subject'] = "Subiect";
$lang['message'] = "Mesaj";
$lang['send'] = "Trimite";
$lang['mail_to'] = "De Teren Este Necesar.";
$lang['mail_valid'] = "De Teren Trebuie Conțin O Valabil E-mail Adresa.";
$lang['mail_subject'] = "Subiect Teren Este Necesar.";
$lang['mail_success'] = "E-mail Trimite Cu Succes%2c";
$lang['mail_error'] = "Oops%2c E-mail Nu Trimite%2c";
$lang['sattendance_submit'] = "Trimite";
$lang['sattendance_present'] = "Prezent";
$lang['sattendance_absent'] = "Absent";
$lang['sattendance_late_present'] = "Târziu Prezent";
$lang['sattendance_late_excuse'] = "Târziu Prezent Cu Scuză";
$lang['sattendance_day']        = "Zi";
$lang['sattendance_classes']    = "Clase";
$lang['sattendance_section']    = "Secțiunea";
$lang['sattendance_subject']    = "Subiect";
$lang['sattendance_monthyear']  = "Monthyear";
$lang['sattendance_attendance'] = "Participarea";
$lang['sattendance_subject'] = "Subiect";
$lang['sattendance_total_mark'] = "Total Marca";
$lang['sattendance_point'] = "Punct";
$lang['sattendance_grade'] = "Clasa";
$lang['sattendance_grade'] = "Clasa";
$lang['sattendance_not_found'] = "Nu  - A Găsit";
$lang['sattendance_report'] = "Prezență Raport";
$lang['sattendance_hotline'] = "Hotline";
$lang['sattendance_total_marks']  = "Total Semne";
$lang['sattendance_average_marks'] = "Medie Semne";
$lang['sattendance_average_grade'] = "Medie Clasa";
$lang['sattendance_average_point'] = "Medie Punct";
$lang['sattendance_type'] = "Tip";
$lang['sattendance_to'] = "Pentru A";
$lang['sattendance_subject'] = "Subiect";
$lang['sattendance_message'] = "Mesaj";
$lang['sattendance_studentID'] = "Student Id";
$lang['sattendance_classesID'] = "Clase Id";
$lang['sattendance_data_not_found'] = "Nu%27t Au Orice Date.";
$lang['sattendance_permissionmethod'] = "Metoda Nu Permis";
$lang['sattendance_permission'] = "Permisiunea Nu Permis";
$lang['sattendance_student'] = "Student";
$lang['sattendance_total_holiday'] = "Total De Vacanță";
$lang['sattendance_total_weekenday'] = "Total Weekenday";
$lang['sattendance_total_present'] = "Total Cadou";
$lang['sattendance_total_latewithexcuse'] = "Total Târziu Cu Scuză";
$lang['sattendance_total_late'] = "Total Târziu";
$lang['sattendance_total_absent'] = "Total Absent";
