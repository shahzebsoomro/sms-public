<?php

$lang['panel_title'] = "Frontend Setări";
$lang['frontend_setting_facebook'] = "Facebook";
$lang['frontend_setting_twitter'] = "Twitter";
$lang['frontend_setting_linkedin'] = "Linkedin";
$lang['frontend_setting_youtube'] = "Youtube";
$lang['frontend_setting_google'] = "Google %2b";
$lang['frontend_setting_frontend_configaration'] = "Frontend Configaration";
$lang['frontend_setting_social'] = "Sociale";
$lang['frontend_setting_description'] = "Descriere";
$lang['frontend_setting_teacher_email'] = "Profesor E-mail";
$lang['frontend_setting_teacher_phone'] = "Profesor Telefon";
$lang['frontend_setting_enable'] = "Permite";
$lang['frontend_setting_disable'] = "Dezactivați";
$lang['update_frontend_setting'] = "Actualizare Frontend Stabilirea";
