<?php

$lang['panel_title'] = "Ubicación";
$lang['add_title'] = "Añadir Un Ubicación";
$lang['slno'] = "#";
$lang['location'] = "Ubicación";
$lang['location_description'] = "Descripción";
$lang['location_add'] = "Agregar";
$lang['action'] = "Acción";
$lang['view'] = "Ver";
$lang['edit'] = "Editar";
$lang['delete'] = "Eliminar";
$lang['print'] = "Imprimir";
$lang['pdf_preview'] = "Pdf Vista Previa";
$lang["mail"] = "Enviar Pdf Para Correo";
$lang['add_location'] = "Añadir Ubicación";
$lang['update_location'] = "Actualización Ubicación";
$lang['to'] = "A";
$lang['subject'] = "Tema";
$lang['message'] = "Mensaje";
$lang['send'] = "Enviar";
$lang['mail_to'] = "Para Campo Es Necesario.";
$lang['mail_valid'] = "Para Campo Debe Contiene Un Válida Correo Electrónico Dirección.";
$lang['mail_subject'] = "El Tema Campo Es Necesario.";
$lang['mail_success'] = "Correo Electrónico Enviar Exitosamente%2c";
$lang['mail_error'] = "Oops%2c Correo Electrónico No Enviar%2c";
