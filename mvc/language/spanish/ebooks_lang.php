<?php

$lang['panel_title'] = "E-libros";
$lang['slno'] = "#";
$lang['ebooks_name'] = "Nombre";
$lang['ebooks_author'] = "Autor";
$lang['ebooks_classes'] = "Clase";
$lang['ebooks_authority'] = "La Autoridad";
$lang['ebooks_private'] = "Privado";
$lang['ebooks_cover_photo'] = "Tapa Foto";
$lang['ebooks_file'] = "Archivo";
$lang['action'] = "Acción";
$lang['ebooks_select_class'] = "Seleccione Clase";
$lang['ebooks_select_department'] = "Seleccione Departamento";
$lang['ebooks_select_teacher'] = "Seleccione Maestro";
$lang['view'] = "Ver";
$lang['edit'] = "Editar";
$lang['delete'] = "Eliminar";
$lang['add_ebooks'] = "Añadir Ebook";
$lang['add_title'] = "Añadir Ebook";
$lang['update_ebooks'] = "Actualización Ebook";
