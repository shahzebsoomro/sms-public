<?php

$lang['panel_title'] = "Activos Categoría";
$lang['add_title'] = "Añadir Los Activos De Categoría";
$lang['slno'] = "#";
$lang['asset_category'] = "Categoría";
$lang['asset_category_add'] = "Agregar";
$lang['action'] = "Acción";
$lang['view'] = "Ver";
$lang['edit'] = "Editar";
$lang['delete'] = "Eliminar";
$lang['print'] = "Imprimir";
$lang['pdf_preview'] = "Pdf Vista Previa";
$lang["mail"] = "Enviar Pdf Para Correo";
$lang['add_asset_category'] = "Añadir Activos De Categoría";
$lang['update_asset_category'] = "Actualización De Activos De Categoría";
$lang['to'] = "A";
$lang['subject'] = "Tema";
$lang['message'] = "Mensaje";
$lang['send'] = "Enviar";
$lang['mail_to'] = "Para Campo Es Necesario.";
$lang['mail_valid'] = "Para Campo Debe Contiene Un Válida Correo Electrónico Dirección.";
$lang['mail_subject'] = "El Tema Campo Es Necesario.";
$lang['mail_success'] = "Correo Electrónico Enviar Exitosamente%2c";
$lang['mail_error'] = "Oops%2c Correo Electrónico No Enviar%2c";
