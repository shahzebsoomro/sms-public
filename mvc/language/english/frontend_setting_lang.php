<?php


$lang['panel_title'] = "Frontend settings";

$lang['frontend_setting_facebook'] = "Facebook";
$lang['frontend_setting_twitter'] = "Twitter";
$lang['frontend_setting_linkedin'] = "Linkedin";
$lang['frontend_setting_youtube'] = "Youtube";
$lang['frontend_setting_google'] = "Google +";

$lang['frontend_setting_frontend_configaration'] = "Frontend Configaration";
$lang['frontend_setting_social'] = "Social";
$lang['frontend_setting_description'] = "Description";
$lang['frontend_setting_teacher_email'] = "Teacher Email";
$lang['frontend_setting_teacher_phone'] = "Teacher Phone";
$lang['frontend_setting_enable'] = "Enable";
$lang['frontend_setting_disable'] = "Disable";

$lang['update_frontend_setting'] = 'Update Frontend setting';