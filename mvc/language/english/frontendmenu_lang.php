<?php

$lang['panel_title'] = "Frontend Menu";
// $lang['add_title'] = "Add a Activities";
// $lang['slno'] = "#";
// $lang['activities_title'] = "Title";
// $lang['activities_description'] = "Description";
// $lang['activities_students'] = "Students";
// $lang['activities_shared_publicly'] = 'Shared Publicly';
// $lang['file_browse'] = "Browse File";
// $lang['attachment'] = "Attachment";
// $lang['clear'] = "Clear";
// $lang['activities_time_frame'] = "Time Frame";
// $lang['activities_time_from'] = "Time from";
// $lang['activities_time_to'] = "Time to";
// $lang['activities_time_at'] = "Time At";
// $lang['students'] = "Student List";
// $lang['activities_create_date'] = "Create Date";
// $lang['action'] = "Action";

// $lang['edit'] = 'Edit';
// $lang['delete'] = 'Delete';

// $lang['add_activities'] = 'Add Activities';
// $lang['update_activities'] = 'Update Activities';




$lang['frontendmenu_edit_menus'] = "Edit Menus";
$lang['frontendmenu_select_a_menu_to_edit'] = "Select a menu to edit";
$lang['frontendmenu_manage_locations'] = "Manage Loacations";
$lang['frontendmenu_top_menu'] = "Top Menu";
$lang['frontendmenu_social_links_menu'] = "Social Links Menu";
$lang['frontendmenu_select'] = "Select";


$lang['frontendmenu_pages'] = "Pages";
$lang['frontendmenu_posts'] = "Posts";
$lang['frontendmenu_custom_links'] = "Custom Links";
$lang['frontendmenu_custom_link'] = "Custom Link";

$lang['frontendmenu_view_all'] = "View All";
$lang['frontendmenu_select_all'] = "Select All";
$lang['frontendmenu_url'] = "URL";
$lang['frontendmenu_link_text'] = "Link Text";
$lang['frontendmenu_pending'] = "Pending";


$lang['frontendmenu_page'] = "Page";
$lang['frontendmenu_post'] = "Post";



$lang['frontendmenu_navigation_label'] = "Navigation Label";
$lang['frontendmenu_move'] = "Move";
$lang['frontendmenu_up_one'] = "Up one";
$lang['frontendmenu_down_one'] = "Down one";
$lang['frontendmenu_orginal'] = "Orginal";
$lang['frontendmenu_no_label'] = "no label";
$lang['frontendmenu_remove'] = "Remove";
$lang['frontendmenu_cancel'] = "Cancel";




$lang['frontendmenu_add_to_menu'] = "Add to Menu";
$lang['frontendmenu_save_menu'] = "Save Menu";
$lang['frontendmenu_save_changes'] = "Save Changes";


$lang['frontendmenu_menu_name'] = "Menu Name";
$lang['frontendmenu_menu_structure'] = "Menu Structure";
$lang['frontendmenu_menu_structure_desc'] = "Drag each item into the order you prefer. Click the arrow on the right of the item to reveal additional configuration options.";
$lang['frontendmenu_display_location'] = "Display location";
$lang['frontendmenu_menu_settings'] = "Menu Settings";
$lang['frontendmenu_delete_menu'] = "Delete Menu";


$lang['frontendmenu_select_a_menu'] = "Select a Menu";


$lang['frontendmenu_error_label'] = 'The label field cannot exceed 253 characters in length.';

$lang['frontendmenu_error_menu'] = 'Please create a menu.';
$lang['frontendmenu_or'] = 'or';
$lang['frontendmenu_create_a_new_menu'] = 'Create a new menu.';
$lang['frontendmenu_create_menu'] = 'Create Menu';
$lang['frontendmenu_create_menu_desc'] = 'Give your menu a name, then click Create Menu.';
$lang['frontendmenu_edit_your_menu_below'] = 'Edit your menu below';


$lang['frontendmenu_error_menu_required'] = 'The menu name field is required.';
$lang['frontendmenu_error_menu_length'] = 'The menu name field cannot exceed 128 characters in length.';
