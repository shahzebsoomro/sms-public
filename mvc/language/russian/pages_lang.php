<?php

$lang['panel_title'] = "С�������";
$lang['add_title'] = "Д������� в с������";
$lang['slno'] = "#";
$lang['pages_content'] = "С���������";
$lang['action'] = "Д�������";
$lang['edit'] = "Р������������";
$lang['delete'] = "У������";
$lang['pages_scheduled'] = "З��������������";
$lang['pages_date'] = "Д���";
$lang['pages_permalink'] = "����������� с�����";
$lang['pages_sm_edit'] = "р������������";
$lang['pages_add_new_page'] = "Д������� Н���� С�������";
$lang['pages_edit_page'] = "Р������������� С�������";
$lang['pages_enter_title_here'] = "В������ н������� з����";
$lang['pages_add_media'] = "Д������� С��";
$lang['pages_save_draft'] = "С�������� �������";
$lang['pages_save_as_pending'] = "С�������� к�� в о�������";
$lang['pages_publish'] = "О�����������";
$lang['pages_update'] = "О���������";
$lang['pages_published'] = "О�����������";
$lang['pages_preview'] = "���������������� п�������";
$lang['pages_preview_changes'] = "���������������� ��������� И��������";
$lang['pages_status'] = "С�����";
$lang['pages_draft'] = "�������";
$lang['pages_edit'] = "Р������������";
$lang['pages_pending_review'] = "Д� К����������";
$lang['pages_ok'] = "Х�����";
$lang['pages_cancel'] = "О�������";
$lang['pages_visibility'] = "В��������";
$lang['pages_public'] = "О�����������";
$lang['pages_password_protected'] = "������� З�������";
$lang['pages_private'] = "Ч������";
$lang['pages_password'] = "�������";
$lang['pages_immediately'] = "с����";
$lang['pages_page_attribute'] = "А������ С�������";
$lang['pages_template'] = "Ш�����";
$lang['pages_set_featured_image'] = "Н���� л����� и����������";
$lang['pages_remove_featured_image'] = "У������ л����� и����������";
$lang['pages_slider_images'] = "С������ И����������";
$lang['pages_set_slider_image'] = "Н���� с������ и����������";
$lang['pages_insert_media'] = "В������ С��";
$lang['pages_insert_into_page'] = "В������� в с�������";
$lang['pages_create_gallery'] = "С������ Г������";
$lang['pages_create_a_new_gallery'] = "С������ в н���� г������";
$lang['pages_create_audio_playlist'] = "С������ А���� ���������";
$lang['pages_create_a_new_playlist'] = "С������ н� н���� п�������";
$lang['pages_create_video_playlist'] = "С������ В���� В��������������";
$lang['pages_create_a_new_video_playlist'] = "С������ в н���� в���� в��������������";
$lang['pages_featured_image'] = "У��������� В И����������";
$lang['pages_set_featured_image'] = "Н���� л����� и����������";
$lang['pages_upload_files'] = "З�������� Ф����";
$lang['pages_media_library'] = "Б��������� М����";
$lang['pages_search_media_items'] = "������ м���� э��������";
$lang['pages_select_file'] = "В������� Ф���";
$lang['pages_maximum_upload_file_size'] = "М����������� ф��� з�������  р�����%3� 8 М�.";
$lang['pages_attachment_details'] = "В������� Д�����";
$lang['pages_delete_permanently'] = "У������ ����������";
$lang['pages_url'] = "Url-�����";
$lang['pages_title'] = "Н�������";
$lang['pages_artist'] = "Х�������";
$lang['pages_album'] = "А�����";
$lang['pages_caption'] = "З��������";
$lang['pages_alt_text'] = "С�������� К����� Alt Т����";
$lang['pages_description'] = "О�������";
$lang['pages_now_playing'] = "С����� И�����";
