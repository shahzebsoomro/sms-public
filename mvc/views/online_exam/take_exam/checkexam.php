<div class="col-sm-12 do-not-refresh">
    <div class="callout callout-danger">
        <h4><?=$this->lang->line('take_exam_warning')?></h4>
        <p><?=$this->lang->line('take_exam_page_refresh')?></p>
    </div>
</div>

<section class="panel">
    <div class="panel-body bio-graph-info">
        <div id="printablediv" class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    
                    <div class="panel-body profile-view-dis">
                        <?php if(count($student)) { ?>
                            <h1><img src="<?=base_url('uploads/images/'.$student->photo)?>" alt="" style="width:100px; height:100px;border:2px solid #CCC" /></h1>
                        <?php } else { ?>
                            <h1><img src="<?=base_url('uploads/images/default.png')?>" alt="" style="width:100px; height:100px;border:2px solid #CCC" /></h1>
                        <?php } ?>
                        
                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_name')?></span>: <?=count($student) ? $student->name : '' ?></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_roll')?></span>: <?=count($student) ? $student->roll : '' ?></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_class')?></span>: <?=count($class) ? $class->classes : '' ?></p>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_section')?></span>: <?=count($section) ? $section->section : '' ?></p>
                            </div>
                        </div>
                    </div>

                    <h2>
                        <?php 
                            if(count($onlineExam) && count($userExamCheck)) {
                                if($onlineExam->markType == 5) {
                                    $percentage = 0;
                                    if($userExamCheck->totalObtainedMark > 0 && $userExamCheck->totalMark > 0) {
                                        $percentage = (($userExamCheck->totalObtainedMark/$userExamCheck->totalMark)*100);
                                    } 

                                    if($percentage >= $onlineExam->percentage) {
                                        echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . '</span>';
                                    } else {
                                        echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . '</span>';
                                    }
                                } elseif($onlineExam->markType == 10) {
                                    if($userExamCheck->totalObtainedMark >= $onlineExam->percentage) {
                                        echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . '</span>';
                                    } else {
                                        echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . '</span>';
                                    }
                                }
                            } 
                        ?>
                    </h2>
                    <?php if(count($userExamCheck)) { ?>
                    <table class="table table-bordered">
                        <tr>
                            <td><?=$this->lang->line('take_exam_total_question')?> : <?=$userExamCheck->totalQuestion?></td>
                            <td><?=$this->lang->line('take_exam_total_answer')?> : <?=$userExamCheck->totalAnswer?></td>
                        </tr>
                        <tr>
                            <td><?=$this->lang->line('take_exam_total_current_answer')?> : <?=$userExamCheck->totalCurrectAnswer?></td>
                            <td><?=$this->lang->line('take_exam_total_mark')?> : <?=$userExamCheck->totalMark?></td>
                        </tr> 
                        <tr>
                            <td><?=$this->lang->line('take_exam_total_obtained_mark')?> : <?=$userExamCheck->totalObtainedMark?></td>
                            <td><?=$this->lang->line('take_exam_total_percentage')?> : <?=$userExamCheck->totalPercentage?> % </td>
                        </tr>
                    </table>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>