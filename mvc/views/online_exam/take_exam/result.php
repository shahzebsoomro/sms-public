<div class="col-sm-12 do-not-refresh">
    <div class="callout callout-danger">
        <h4><?=$this->lang->line('take_exam_warning')?></h4>
        <p><?=$this->lang->line('take_exam_page_refresh')?></p>
    </div>
</div>

<section class="panel">
    <div class="panel-body bio-graph-info">
        <div id="printablediv" class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    
                    <div class="panel-body profile-view-dis">
                        <?php if(count($student)) { ?>
                            <h1><img src="<?=base_url('uploads/images/'.$student->photo)?>" alt="" style="width:100px; height:100px;border:2px solid #CCC" /></h1>
                        <?php } else { ?>
                            <h1><img src="<?=base_url('uploads/images/default.png')?>" alt="" style="width:100px; height:100px;border:2px solid #CCC" /></h1>
                        <?php } ?>
                        
                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_name')?></span>: <?=count($student) ? $student->srname : '' ?></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_roll')?></span>: <?=count($student) ? $student->srroll : '' ?></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_class')?></span>: <?=count($class) ? $class->classes : '' ?></p>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_section')?></span>: <?=count($section) ? $section->section : '' ?></p>
                            </div>
                        </div>
                    </div>

                    <h2>
                        <?php 
                            if(count($onlineExam)) {
                                if($onlineExam->markType == 5) {
                                    $percentage = 0;
                                    if($totalCorrectMark > 0 && $totalQuestionMark > 0) {
                                        $percentage = (($totalCorrectMark/$totalQuestionMark)*100);
                                    } 

                                    if($percentage >= $onlineExam->percentage) {
                                        echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . '</span>';
                                    } else {
                                        echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . '</span>';
                                    }
                                } elseif($onlineExam->markType == 10) {
                                    if($totalCorrectMark >= $onlineExam->percentage) {
                                        echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . '</span>';
                                    } else {
                                        echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . '</span>';
                                    }
                                }
                            } 
                        ?>
                    </h2>
                    <table class="table table-bordered">
                        <tr>
                            <td><?=$this->lang->line('take_exam_total_question')?> : <?=count($onlineExamQuestions)?></td>
                            <td><?=$this->lang->line('take_exam_total_answer')?> : <?=$totalAnswer?></td>
                        </tr>
                        <tr>
                            <td><?=$this->lang->line('take_exam_total_current_answer')?> : <?=$correctAnswer?></td>
                            <td><?=$this->lang->line('take_exam_total_mark')?> : <?=$totalQuestionMark?></td>
                        </tr> 
                        <tr>
                            <td><?=$this->lang->line('take_exam_total_obtained_mark')?> : <?=$totalCorrectMark?></td>
                            <td><?=$this->lang->line('take_exam_total_percentage')?> : <?=($totalCorrectMark > 0 && $totalQuestionMark > 0) ? number_format((($totalCorrectMark/$totalQuestionMark)*100),2) .'%': '0%' ?></td>
                        </tr>
                    </table>
                    
                    <?php if(count($userExamCheck)) { foreach($userExamCheck as $examCheck) { ?>
                    <h2>
                    <?php 
                        if(count($onlineExam)) {
                            if($onlineExam->markType == 5) {
                                $percentage = 0;
                                if($examCheck->totalObtainedMark > 0 && $examCheck->totalMark > 0) {
                                    $percentage = (($examCheck->totalObtainedMark/$examCheck->totalMark)*100);
                                } 

                                if($percentage >= $onlineExam->percentage) {
                                    echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . '</span>';
                                } else {
                                    echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . '</span>';
                                }
                            } elseif($onlineExam->markType == 10) {
                                if($examCheck->totalObtainedMark >= $onlineExam->percentage) {
                                    echo '<span class="text-green">'. $this->lang->line('take_exam_pass') . '</span>';
                                } else {
                                    echo '<span class="text-red">'. $this->lang->line('take_exam_fail') . '</span>';
                                }
                            }
                        } 
                    ?>
                    </h2>
                    <table class="table table-bordered">
                        <tr>
                            <td><?=$this->lang->line('take_exam_total_question')?> : <?=$examCheck->totalQuestion?></td>
                            <td><?=$this->lang->line('take_exam_total_answer')?> : <?=$examCheck->totalAnswer?></td>
                        </tr>
                        <tr>
                            <td><?=$this->lang->line('take_exam_total_current_answer')?> : <?=$examCheck->totalCurrectAnswer?></td>
                            <td><?=$this->lang->line('take_exam_total_mark')?> : <?=$examCheck->totalMark?></td>
                        </tr> 
                        <tr>
                            <td><?=$this->lang->line('take_exam_total_obtained_mark')?> : <?=$examCheck->totalObtainedMark?></td>
                            <td><?=$this->lang->line('take_exam_total_percentage')?> : <?=number_format($examCheck->totalPercentage,2)?> % </td>
                        </tr>
                    </table>
                    <?php } } ?>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" >
    $('.sidebar-menu li a').css('pointer-events', 'none');

    // $('.left-side').hide();
    // $('.right-side').css('margin-left', '0px');
    // $('.header').hide();

    function disableF5(e) {
        if ( ( (e.which || e.keyCode) == 116 ) || ( e.keyCode == 82 && e.ctrlKey ) ) {
            e.preventDefault();
        }
    }

    $(document).bind("keydown", disableF5);

    function Disable(event) {
        if (event.button == 2)
        {
            window.oncontextmenu = function () {
                return false;
            }
        }
    }
    document.onmousedown = Disable;
</script>