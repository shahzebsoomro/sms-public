<div class="col-sm-12 do-not-refresh">
    <div class="callout callout-danger">
        <h4><?=$this->lang->line('take_exam_warning')?></h4>
        <p><?=$this->lang->line('take_exam_page_refresh')?></p>
    </div>
</div>

<section class="panel">
    <div class="panel-body bio-graph-info">
        <div id="printablediv" class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    
                    <div class="panel-body profile-view-dis">
                        <?php if(count($student)) { ?>
                            <h1><img src="<?=base_url('uploads/images/'.$student->photo)?>" alt="" style="width:100px; height:100px;border:2px solid #CCC" /></h1>
                        <?php } else { ?>
                            <h1><img src="<?=base_url('uploads/images/default.png')?>" alt="" style="width:100px; height:100px;border:2px solid #CCC" /></h1>
                        <?php } ?>
                        
                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_name')?></span>: <?=count($student) ? $student->name : '' ?></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_roll')?></span>: <?=count($student) ? $student->roll : '' ?></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_class')?></span>: <?=count($class) ? $class->classes : '' ?></p>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="profile-view-tab">
                                <p><span><?=$this->lang->line('take_exam_section')?></span>: <?=count($section) ? $section->section : '' ?></p>
                            </div>
                        </div>
                    </div>

                    <?php 
                        if(count($onlineExam)) {
                            if($expirestatus) {
                                echo '<h2>'.$this->lang->line('take_exam_expired').'<h2>';
                            } elseif($upcomingstatus) {
                                echo '<h2>'.$this->lang->line('take_exam_upcoming').'</h2>';
                            }
                        } else {
                            echo "<h2>".$this->lang->line('take_exam_exam_not_found')."</h2>";
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>